#include <iostream>
#include <math.h>

using namespace std;

//Ejercicios

//Ejercicio 1
/*
void fun_a(int *px, int *py);
void fun_b(int a[], int tam);
int main(){
    int array[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    fun_b(array, 10);
    cout<<&array;
    }

void fun_a(int *px, int *py){
    int tmp = *px;
    *px = *py;
    *py = tmp;
}
void fun_b(int a[], int tam){
    int f, l;
    int *b = a;
    for (f = 0, l = tam -1; f < l; f++, l--) {
    fun_a(&b[f], &b[l]);
    }
}
//Direccion de array 0x7ffd41a76e20 y cada elemento del array ocupa 4 bytes
//Direccion de array[3] (&array[3]) 0x7ffd41a76e2c y depende si es antes o despues de implementar la funcion (antes es 3 y luego es 6)}
//La funcion invierte el orden del arreglo
*/

//Ejercicio 2
/*
void fun_c(double *a, int n, double *promedio, double *suma);

int main(){
    double *av;
    double *sum;
    int n=5;
    double a[n]={1,2,3,4,5};
    fun_c(a,n,av,sum);
    cout<<"El promedio de notas es "<<*av<<endl;
    return 0;

}

void fun_c(double *a, int n, double *promedio, double *suma){
    *suma=0.0;
    for (int i=0;i<n;i++){
        *suma+=*(a+i);}
        *promedio=*suma/n;
} */

//Ejercicio 3
/*
int main(){
    int a[2][2]={{0,1},{2,3}};
    cout<<a<<endl;
    cout<<(*a++)<<endl;
}
//0x0A12-0x0A16-0x0A1A-0x0A1E-0x0A22-0x0A26-0x0A2A-0x0A2E
//0x0A12
//0x0A22
//0x0A22
//0X0a26
//A (lo que haya ahi)
//A
//
*/

//Ejercicio 4
/*                                  //Circuito en tinkercad
int sensor;
double voltaje;

void setup(){
    pinMode(3,OUTPUT);				//Iniciamos el pin 3 como salida
    Serial.begin(9600); 			//Inicia el puerto serial
    pinMode(13,OUTPUT); 			//Inicia el pin 13 (led) como escritura
}

void loop(){

  sensor=analogRead(A0); 			//En sensor se lee el valor del voltaje de entrada
  voltaje=sensor*5.0/1023.0; 		//Convierte el valor que esta llegando a valores de voltaje
  Serial.println(sensor); 			//Imprime el valor (analogico) que esta llegando al arduino (valor de voltaje)
  Serial.println(voltaje); 			//Imprime el valor del voltaje converitdo
  if (voltaje>3){       			//Si el valor de voltaje es mayor a 3 prende el led
    digitalWrite(13,HIGH);
  }
  else {							//De lo contrario lo apaga
    digitalWrite(13,LOW);
  }

  sensor=map(sensor,0,1023,0,255);	//La escritura en un pwm se hace con 8 bits mientras que la lectura analogica es con 10
  analogWrite(3,sensor);			//Escribe en el pin 3 el valor que le llegue de sensor
} */

//Ejercicio 5
/*                                  //Circuito en tinkercad
// include the library code:
#include <LiquidCrystal.h>

// Constantes para la inicialización del LCD
#define RS 12
#define ENABLE 11
#define d4 5
#define d5 4
#define d6 3
#define d7 2
#define FILAS 2
#define COLUMNAS 16
#define BASE 10

// Puerto analógico 0
#define A0 0


// https://www.arduino.cc/en/Reference/LiquidCrystalConstructor
LiquidCrystal lcd(RS, ENABLE, d4, d5, d6, d7);

int val=0;
int iluminacion = 0;
int incomingByte = 0;   // for incoming serial data

void setup() {
  // Establece numero de filas y columnas
  lcd.begin(COLUMNAS, FILAS);
  Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps
}

void loop() {
  // Cambio posición cursor
  lcd.setCursor(0, 0);
  lcd.print("En este momento");
  lcd.setCursor(0, 1);
  val = analogRead(A0);
  iluminacion =map(val,54, 974, 0, 100);
  String textoLCD = String("hay " + String(iluminacion, BASE) + "% de luz     ");
  lcd.println(textoLCD);

  Serial.write(45); // send a byte with the value 45
  Serial.println(45);

  // send data only when you receive data:
  if (Serial.available() > 0) {
  // read the incoming byte:
    incomingByte = Serial.parseInt();

    int re = (int)incomingByte;

    // say what you got:
    Serial.print("I received: ");
    Serial.write(incomingByte);
    Serial.println("");
    //int bytesSent = Serial.write("hello"); //send the string "hello" and return the length of the string.

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("I received: ");
    lcd.setCursor(0, 1);
    lcd.print((char)incomingByte);
    delay(500);
  }
} */

//Problemas

//Problema 1
/*
int main(){
    int bill[11]={50000,20000,10000,5000,2000,1000,500,100,50};
    int mon;
    cout<<"Ingrese una cantidad de dinero "<<endl;
    cin>>mon;
    for (int i=0;i<=10;i++){
        cout<<bill[i]<<":"<<mon/bill[i]<<endl;
        mon=mon%bill[i];
        }
    cout<<"Faltante:"<<mon<<endl;
    return 0;
} */

//Problema 2
/*
#include <cstdlib>
#include <time.h>
int main(){
    srand (time(0));
    char arrayabc[26];
    int array[200];
    int ran;
    int con=0;
    for (int k=0;k<=25;k++){
        arrayabc[k]=k+65;
    }
    for (int i=0;i<=199;i++){
        ran=65+rand()%26;
        array[i]=ran;
    }
    for (int j=0;j<=199;j++){
        cout<<array[j]<<" ";
    }
    cout<<endl;
    for (int i=0;i<=25;i++){
        for (int j=0;j<=199;j++){
            if (arrayabc[i]==array[j]){
                con++;
            }
        }
        cout<<arrayabc[i]<<":"<<con<<endl;
        con=0;
    }
    return 0;
} */

//Problema 3
/*
bool comp(char a[],char b[],int len);

int main(){
    char a[5]={'a','b','c','d','e'};
    char b[5]={'a','e','i','o','u'};
    if (comp(a,b,5)){
        cout<<"Son iguales"<<endl;
    }
    else{
        cout<<"No son iguales"<<endl;
    }
    return 0;
}

bool comp(char a[], char b[], int len){
    for (int i=0;i<=len;i++){
        if (a[i]!=b[i]){
            return false;
        }
    }
    return true;
} */

//Problema 4
/*
int strtoint (char str[]);

int main(){
    char str[]={};
    int num;
    cout<<"Ingrese una cadena de caracteres numericos "<<endl;
    cin>>str;
    num=strtoint(str);
    cout<<num<<endl;
    return 0;
}

int strtoint (char str[]){
    int tam=0;
    int num=0;
    while (1){
        if (*(str+tam)=='\0'){
            break;
        }
        tam++;
    }
    for (int i=0;i<tam;i++){
        num=(num)*10+(*(str+i)-48);
    }
    return num;
} */

//Problema 5
/*
void inttostr(int num, char str[]);

int main(){
    int num;
    char str[]={};
    cout<<"Ingrese un numero "<<endl;
    cin>>num;
    inttostr(num, str);
    cout<<str<<endl;
}

void inttostr(int num, char str[]){
    int tam=0;
    int aux;
    aux=num;
    while (aux!=0){
        aux=aux/10;
        tam++;
    }
    for (int i=0, j=(tam-1);i<tam;i++,j--){
        *(str+i)=48+(num/pow(10,j));
        num=(num)%int(pow(10,j));
    }
    *(str+tam)='\0';
} */

//Problema 6
/*
int main(){
    char str[]={};
    int  con=0;
    cout<<"Ingrese una cadena de caracteres (sin espacios) "<<endl;
    cin>>str;
    while (*(str+con)!='\0'){
        if (*(str+con)<=122&&*(str+con)>=97){
            *(str+con)-=32;
        }
        con++;
    }
    cout<<str<<endl;
    return 0;
} */

//Problema 7
/*
int main(){
    char str[10]={};
    char strnew[10]={};
    int  con1=0;
    int  con2=0;
    bool flag=1;
    cout<<"Ingrese una cadena de caracteres (sin espacios) "<<endl;
    cin>>str;
    while (*(str+con1)!='\0'){
        flag=1;
        for (int i=0;i<=con2;i++){
            if (*(str+con1)==*(strnew+i)){
                flag=0;
                break;
            }
        }
        if (flag){
            *(strnew+con2)=*(str+con1);
            con2++;
        }
        con1++;
    }
    *(strnew+con2)='\0';
    cout<<strnew<<endl;
    return 0;

} */

//Problema 8
/*
int main(){
    char str[10]={};
    char strnum[10]={};
    char strlet[10]={};
    int  con1=0;
    int  con2=0;
    int con3=0;
    cout<<"Ingrese una cadena de caracteres (sin espacios) "<<endl;
    cin>>str;
    while (*(str+con1)!='\0'){
        if(*(str+con1)<=57&&*(str+con1)>=48){
            *(strnum+con2)=*(str+con1);
            con2++;
        }
        else{
            *(strlet+con3)=*(str+con1);
            con3++;
        }
        con1++;
    }
    *(strnum+con2)='\0';
    *(strlet+con3)='\0';
    cout<<strnum<<endl;
    cout<<strlet<<endl;
    return 0;
} */

//Problema 9
/*
int main(){
    char *str;
    int len=0;
    int cifras;
    int aux;
    int curr=0;
    int sum=0;
    cout<<"Digite una cadena de caracteres numericos "<<endl;
    cin>>str;
    cout<<"Ingrese un numero"<<endl;
    cin>>cifras;
    while(*(str+len)!='\0'){
        len++;
    }
    for(int i=0;i<(cifras-(len%cifras));i++){
        str=str-1;
        *(str)='0';
    }
    if(len%cifras!=0){
        aux=(len/cifras)+1;
    }
    for(int i=0;i<aux;i++){
        for(int j=0;j<cifras;j++){
            curr=curr*10+(*(str+j+(i*cifras))-48);
        }
        sum=sum+curr;
        curr=0;
    }
    cout<<sum<<endl;
    return 0;
} */

//Problema 10
/*
int main(){
    char rom[10]={};
    cout<<"Ingrese un numero romano "<<endl;
    cin>>rom;
    int sum=0;
    int rest=0;
    int total;
    int len=0;
    while(*(rom+len)!='\0'){
        len++;
    }
    cout<<len<<endl;
    for(int i=0;i<len;i++){
        if(*(rom+i)=='I'&&((*(rom+i+1))=='V'||(*(rom+i+1))=='X'||(*(rom+i+1))=='L'||(*(rom+i+1))=='C'||(*(rom+i+1))=='D'||(*(rom+i+1))=='M')){
            rest=rest+1;
        }
        else if (*(rom+i)=='I'){
            sum=sum+1;
        }
        if (*(rom+i)=='V'&&((*(rom+i+1))=='X'||(*(rom+i+1))=='L'||(*(rom+i+1))=='C'||(*(rom+i+1))=='D'||(*(rom+i+1))=='M')){
            rest=rest+5;
        }
        else if (*(rom+i)=='V'){
            sum=sum+5;
        }
        if (*(rom+i)=='X'&&((*(rom+i+1))=='L'||(*(rom+i+1))=='C'||(*(rom+i+1))=='D'||(*(rom+i+1))=='M')){
            rest=rest+10;
        }
        else if (*(rom+i)=='X'){
            sum=sum+10;
        }
        if (*(rom+i)=='L'&&((*(rom+i+1))=='C'||(*(rom+i+1))=='D'||(*(rom+i+1))=='M')){
            rest=rest+50;
        }
        else if (*(rom+i)=='L'){
            sum=sum+50;
        }
        if (*(rom+i)=='C'&&((*(rom+i+1))=='D'||(*(rom+i+1))=='M')){
            rest=rest+100;
        }
        else if (*(rom+i)=='C'){
            sum=sum+100;
        }
        if (*(rom+i)=='D'&&((*(rom+i+1))=='M')){
            rest=rest+500;
        }
        else if (*(rom+i)=='D'){
            sum=sum+500;
        }
        if (*(rom+i)=='M'){
            sum=sum+1000;
        }
    }
    total=sum-rest;
    cout<<total<<endl;
    return 0;
} */

//Problema 11

//Parte 1
/*
void showsala(bool sala[15][20]);
void reserva(bool sala[15][20],char fila,int numero,int begin);
int main(){
    bool flag=1;
    char flagc;
    bool sala[15][20];
    for(int i=0;i<15;i++){
        for(int j=0;j<20;j++){
            sala[i][j]=0;
        }
    }
    showsala(sala);
    char fila;
    int asientos;
    int begin;
    while (flag){
        cout<<"Ingrese la fila en la que desea realizar la reservacion "<<endl;
        cin>>fila;
        cout<<"Ingrese el numero de asientos que desea reservar "<<endl;
        cin>>asientos;
        cout<<"Ingrese a partir de que asiento desea realizar la reserva "<<endl;
        cin>>begin;
        if(asientos+begin>20||(fila>=80||fila<=64)){
            cout<<"La reservacion no es valida "<<endl;
            cout<<"Ingrese la fila en la que desea realizar la reservacion "<<endl;
            cin>>fila;
            cout<<"Ingrese el numero de asientos que desea reservar "<<endl;
            cin>>asientos;
            cout<<"Ingrese a partir de que asiento desea realizar la reserva "<<endl;
            cin>>begin;
        }
        reserva(sala,fila,asientos,begin);
        cout<<"Desea realizar otra reserva? (Y/N)"<<endl;
        cin>>flagc;
        if (flagc=='N'){
            flag=0;
        }
    }
}

void showsala(bool sala[15][20]){
    char fila[15]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'};
    int columna[20]={1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0};
    for (int i=0;i<20;i++){
        cout<<"  "<<columna[i];
    }
    cout<<endl;
    for(int i=0;i<15;i++){
        cout<<fila[i];
        for(int j=0;j<20;j++){
            cout<<" ";
            if (sala[i][j]){
                cout<<"+ ";
            }
            else{
                cout<<"- ";
            }
        }
        cout<<endl;
    }
}

void reserva(bool sala[15][20],char fila,int numero,int begin){
    int nfila;
    nfila=fila-65;
    for(int i=begin-1;i<begin+numero-1;i++){
        if (sala[nfila][i]==1){
            cout<<"Asientos ya reservados"<<endl;
            return ;
        }
    }
    for(int i=begin-1;i<begin+numero-1;i++){
        sala[nfila][i]=1;
    }
    showsala(sala);
} */

//Parte 2
/*
int showtablerof1(bool tablero[10][10]);
int showtablerof2(bool tablero[10][10], bool tablero2[10][10]);
void setbarcos(bool tablero[10][10],char fila,int columna);
void setatck(bool tablero2[10][10],char fila,int columna);
int main(){
    bool flag=1;
    char flagc;
    bool tablero[10][10];
    int intentos=0;
    int hits;
    int barcos;
    for(int i=0;i<15;i++){
        for(int j=0;j<10;j++){
            tablero[i][j]=0;
        }
    }
    barcos=showtablerof1(tablero);
    char fila;
    int columna;
    while (flag){
        cout<<"Ingrese la fila en la que desea ubicar el barco (A-J)"<<endl;
        cin>>fila;
        cout<<"Ingrese la columna en la que desea ubicar el barco (1-10)"<<endl;
        cin>>columna;
        if(columna<1||columna>10||(fila>=74||fila<=64)){
            cout<<"La ubicacion no es valida "<<endl;
            cout<<"Ingrese la fila en la que desea ubicar el barco (A-J)"<<endl;
            cin>>fila;
            cout<<"Ingrese la columna en la que desea ubicar el barco (1-10)"<<endl;
            cin>>columna;
        }
        setbarcos(tablero,fila,columna);
        barcos=showtablerof1(tablero);
        cout<<"Desea ubicar otro barco? (Y/N)"<<endl;
        cin>>flagc;
        if(flagc=='N'){
            flag=0;
        }
    }
    cout<<"Hay "<<barcos<<" barcos para derribar."<<endl;
    bool tablero2[10][10];
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            tablero2[i][j]=0;
        }
    }
    while(hits!=barcos){
        cout<<"Ingrese la fila en la que desea disparar (A-J)"<<endl;
        cin>>fila;
        cout<<"Ingrese la columna en la que desea disparar (1-10)"<<endl;
        cin>>columna;
        if(columna<1||columna>10||(fila>=74||fila<=64)){
            cout<<"La ubicacion no es valida "<<endl;
            cout<<"Ingrese la fila en la que desea disparar (A-J)"<<endl;
            cin>>fila;
            cout<<"Ingrese la columna en la que desea disparar (1-10)"<<endl;
            cin>>columna;
        }
        intentos++;
        setatck(tablero2,fila,columna);
        hits=showtablerof2(tablero,tablero2);
        cout<<"Faltan "<<barcos-hits<<" barcos por derribar"<<endl;
    }
    cout<<"Eran "<<barcos<<" barcos y requirio "<<intentos<<" intentos"<<endl;
    return 0;
}


int showtablerof1(bool tablero[10][10]){
    char fila[10]={'A','B','C','D','E','F','G','H','I','J'};
    int columna[10]={1,2,3,4,5,6,7,8,9,0};
    int barcos=0;
    for (int i=0;i<10;i++){
        cout<<"  "<<columna[i];
    }
    cout<<endl;
    for(int i=0;i<10;i++){
        cout<<fila[i];
        for(int j=0;j<10;j++){
            cout<<" ";
            if (tablero[i][j]){
                cout<<"+ ";
                barcos++;
            }
            else{
                cout<<"- ";
            }
        }
        cout<<endl;
    }
    return barcos;
}

int showtablerof2(bool tablero[10][10], bool tablero2[10][10]){
    char fila[10]={'A','B','C','D','E','F','G','H','I','J'};
    int columna[10]={1,2,3,4,5,6,7,8,9,0};
    int hits=0;
    for (int i=0;i<10;i++){
        cout<<"  "<<columna[i];
    }
    cout<<endl;
    for(int i=0;i<10;i++){
        cout<<fila[i];
        for(int j=0;j<10;j++){
            cout<<" ";
            if (tablero[i][j]==1&&tablero2[i][j]==1){
                cout<<"x ";
                hits++;
            }
            else if (tablero2[i][j]==1&&tablero[i][j]==0){
                cout<<"o ";
            }
            else {
                cout<<"- ";
            }
        }
        cout<<endl;
    }
    return hits;
}

void setbarcos(bool tablero[10][10],char fila,int columna){
    int nfila;
    nfila=fila-65;
    if(tablero[nfila][columna-1]==1){
        cout<<"Ahi ya hay un barco"<<endl;
        return ;
    }
    tablero[nfila][columna-1]=1;
    return ;
}
void setatck(bool tablero2[10][10], char fila, int columna){
    int nfila;
    nfila=fila-65;
    if(tablero2[nfila][columna-1]==1){
        cout<<"Ahi ya ha atacado"<<endl;
        return ;
    }
    tablero2[nfila][columna-1]=1;
    return ;
} */

//Problema 12
/*
int main(){
    int large;
    bool flag=1;
    int constant=0;
    int actualc=0;
    int actualf=0;
    int diag=0;
    cout<<"Ingrese la longitu de la matriz "<<endl;
    cin>>large;
    int matriz[large][large];
    for (int i=0;i<large;i++){
        for (int j=0;j<large;j++){
            cout<<"Ingrese el valor en la posicion ("<< i+1 <<","<< j+1 <<") de la matriz"<<endl;
            cin>>matriz[i][j];
        }
    }
    for (int i=0;i<large;i++){
        constant=constant+matriz[i][i];
    }
    for (int i=0;i<large;i++){
        for (int j=0;j<large;j++){
            actualc=actualc+matriz[j][i];
            actualf=actualf+matriz[i][j];
        }
        if (actualc!=constant||actualf!=constant){
            flag=0;
            break;
        }
        actualc=0;
        actualf=0;
    }
    for(int i=0,j=large-1;i<large,j>=0;i++,j--){
        diag=diag+matriz[j][i];
    }
    if (diag!=constant){
        flag=0;
    }
    for (int i=0;i<large;i++){
        for(int j=0;j<large;j++){
            for (int k=0;k<large;k++){
                for (int p=0;p<large;p++){
                    if (matriz[i][j]==matriz[k][p]&&i!=k&&j!=p){
                        flag=0;
                        break;
                    }
                }
            }
        }
    }
    if (flag){
        cout<<"Es un cuadrado magico "<<endl;
    }
    else {
        cout<<"No es un cuadrado magico "<<endl;
    }
} */

//Problema 13
/*
int funcion(int n[][6]);
int main(){
    int a[8][6]={{0,5,2,0,0,5},{3,13,6,0,0,0},{4,6,2,4,7,6},{0,0,7,15,13,10},{0,0,3,4,6,6},{0,0,0,1,9,4},{6,2,10,6,10,8},{8,3,0,0,4,0}};
    int b;
    b=funcion(a);
    cout<<"El numero de estrellas es: "<<b<<endl;
    return 0;
}

int funcion(int n[][6]){
    int c=0;
    int v=0;
    for (int i=1;i<=7;i=i+1){
        for (int j=1;j<=5;j=j+1){
            v=(n[i][j]+n[i][j-1]+n[i][j+1]+n[i-1][j]+n[i+1][j])/5;
            if (v>6){
                c=c+1;}
            v=0;}}
    return c;
} */

//Problema 14
/*
void rot(int matriz[5][5],int matrizh[5][5]);
void printmat(int matriz[5][5]);

int main(){
    int num=1;
    int matriz[5][5];
    int matrizh[5][5];
    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            matriz[i][j]=num;
            num++;
        }
    }
    cout<<"Original: "<<endl;
    printmat(matriz);
    cout<<"90 grados: "<<endl;
    rot(matriz,matrizh);
    cout<<"180 grados: "<<endl;
    rot(matrizh,matriz);
    cout<<"270 grados: "<<endl;
    rot(matriz,matrizh);
}

void rot(int matriz[5][5],int matrizh[5][5]){
    for (int i=0, p=4;i<5;i++,p--){
        for(int j=0,k=0;j<5;j++,k++){
            matrizh[k][p]=matriz[i][j];
        }
    }
    printmat(matrizh);
}

void printmat(int matriz[5][5]){
    for (int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            cout<<matriz[i][j]<<"  ";
        }
        cout<<endl;
    }
} */

//Problema 15
/*
void interseccion(int *A,int *B,int *C);
int main(){
    int A[4], B[4], C[4];
    interseccion(A,B,C);
}

void interseccion(int *A,int *B,int *C){
    for (int i=1 ; i<=2 ; i++){
        cout << "Ingrese los datos para el rectangulo " << i << " : " << endl;
        if ( i==1 ){
            for (int j=0 ; j<4 ; j++){
                cin >> A[j];
            }
        }
        else if ( i == 2){
            for (int j=0 ; j<4 ; j++){
                cin >> B[j];
            }
        }
    }
    if ( (A[0]+A[2])>B[0] && A[0]<B[0]){

        C[0]=B[0];
        C[2]=A[2]-B[0];
    }
    else if ( (B[0]+B[2])>A[0] && B[0]<A[0]){

        C[0]=A[0];
        C[2]=B[2]-A[0];
    }
    else{
        cout << "No se intersectan los rectangulos." << endl;
        exit(0);
    }

    if ( (A[1]+A[3])>B[1] && A[1]<B[1]){
        C[1]=B[1];
        C[3]=A[3]-B[1];
    }
    else if ( (B[1]+B[3])>A[1] && B[1]<A[3]){
        C[1]=A[1];
        C[3]=B[3]-A[1];
    }
    else{
        cout << "No se intersectan los rectangulos." << endl;
        exit(0);
    }

    cout << "El rectangulo de interseccion quedaria expresado como: " << endl;
    for (int i=0 ; i<4 ; i++){
        cout << C[i] <<"  ";
    }
} */

//Problema 16
/*
int main(){
    int n;
    cout<<"Ingrese el largo de la cuadricula: "<<endl;
    cin>>n;
    int C=1;                                                //Numero maximo de permutaciones segun el largo de la cuadricula
    for (int i=2*n;i>0;i--){
        C=C*i;
    }
    int E=0;                                                //Numero maximo de errores (por direccion) segun el largo de la cuadricula
    for (int i=0;i<n;i++){
        E=E+i;
    }
    int EC=0;                                               //Numero de errores por combinacion posible

    for (int i=1;i<=E;i++){
        EC=EC+i;
    }
    EC=(EC*2)+E+1;
    int W;                                                  //Numero de caminos validos
    W=C/EC;
    cout<<"Para una malla de "<<n<<"x"<<n<<" puntos hay "<<W<<" caminos."<<endl;
    return 0;
} */

//Problema 17
/*
bool frdly(int a,int b);
int main(){
    int lim;
    int sum=0;
    cout<<"Ingrese un numero "<<endl;
    cin>>lim;
    int *list;
    list=new int(lim-1);
    for(int i=0;i<lim-1;i++){
        *(list+i)=i+1;
    }
    for (int i=0;i<lim-1;i++){
        for (int j=0;j<lim-1;j++){
            if (frdly(list[i],list[j])){
                sum=sum+list[i];
            }
        }
    }
    cout<<"El resultado de la suma es "<<sum<<endl;
    delete[] list;
    return 0;
}
bool frdly(int a,int b){
    int sdiva=0;
    int sdivb=0;
    for (int i=1;i<a;i++){
        if (a%i==0){
            sdiva=sdiva+i;
        }
    }
    for (int i=1;i<b;i++){
        if (b%i==0){
            sdivb=sdivb+i;
        }
    }
    if (sdiva==b&&sdivb==a){
        return true;
    }
    else{
        return false;
    }
} */

//Problema 18





