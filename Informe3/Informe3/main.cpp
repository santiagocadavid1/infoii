#include <iostream>

using namespace std;

void stringtobinary(char*,char*);
void binarytostring(char*,char*);
void inttobinary(int,char*);
int binarytoint(char*);
void frstmethod(int,char*);
void scndmethod(int,char*);
void invtodo(int,char*);
void invc2(int,char*);
void invc3(int,char*);
void mv(int,char*);
void mvr(int,char*);
void uncodifie1(int pack,char* cfied);
void uncodifie2(int pack,char* cfied);
int lenstr(char*);
int nor(int,char*);

#include <fstream>

//Main para codificar
/*
int main()
{
    int pack;
    char *str;
    char *bi;
    cout<<"Ingrese la longitud que tendra cada paquete "<<endl;
    cin>>pack;
    char cadena[15]=" ";
    ifstream fin;
    ofstream fout;
    ofstream foutc;
    fin.open("mensaje.txt");
    fout.open("binario.txt");
    foutc.open("codified.txt");
    while(fin.good()){
        fin.getline(cadena,15);
        if(fin.good()){
            str=cadena;
            stringtobinary(str,bi);
            fout<<bi<<'\0'<<endl;
            frstmethod(pack,bi);
            foutc<<bi<<'\0'<<endl;
        }
    }
    fout.close();
    fin.close();
    foutc.close();
    return 0;
} */

//Main para descodificar

int main(){
    //Obtener el texto codificado desde un archivo de texto (1 linea)
    char str[7]=" ";
    int pack;
    cout<<"Ingrese la longitud de los paquetes "<<endl;
    cin>>pack;
    char cfied[7*8]=" ";
    ifstream fin;
    fin.open("codified.txt");
    fin.getline(cfied,5*8);
    fin.close();
    cout<<cfied<<endl;
    cout<<"10110100001111010001011010011011"<<endl;
    uncodifie1(pack,cfied);
    cout<<cfied<<endl;
    cout<<"01000001011000100100001101100100"<<endl;
    binarytostring(cfied,str);
    cout<<str<<endl;
    return 0;
}

// Funciones para pasar de string a binario o binario a string

void stringtobinary(char* string,char* bi){
    int temp;
    int con=0;
    while(*(string+con)!='\0'){
        temp=*(string+con);
        inttobinary(temp,(bi+(con*8)));
        con++;
    }
    return ;
}

void binarytostring(char* bi,char* string){
    int con=lenstr(bi)/8;
    for(int i=0;i<con;i++){
        *(string+i)=char(binarytoint(bi+i*8));
    }
}

void inttobinary(int temp,char* bi){
    for (int i=0;i<8;i++){
        if (temp%2==0){
            *(bi+7-i)='0';
        }
        else {
            *(bi+7-i)='1';
        }
        temp=temp/2;
    }
    return ;
}

int binarytoint(char* bi){
    int temp=0;
    int aux=1;
    for(int i=0;i<8;i++){
        if (*(bi+i)=='1'){
            for(int j=0;j<7-i;j++){
                aux=aux*2;
            }
            temp=temp+aux;
            aux=1;
        }
    }
    return temp;
}

//Funciones para encriptar

void frstmethod(int pack,char* bi){
    int lenbi=lenstr(bi);
    int temp=0;
    int con=0;
    temp=nor(pack,(bi+con));
    invtodo(pack,(bi+con));
    con=con+pack;
    for (int i=1;i<(lenbi/pack);i++){
        if((pack/2==temp)&&(pack%2==0)){
            temp=nor(pack,(bi+con));
            invtodo(pack,(bi+con));
            con=con+pack;
        }
        else if (((pack/2>=temp)&&(pack%2==1))||(pack/2>temp)){
            temp=nor(pack,(bi+con));
            invc2(pack,(bi+con));
            con=con+pack;
        }
        else{
            temp=nor(pack,(bi+con));
            invc3(pack,(bi+con));
            con=con+pack;
        }
    }
}

void scndmethod(int pack, char* bi){
    int lenbi=lenstr(bi);
    int con=0;
    for (int i=0;i<(lenbi/pack);i++){
        mv(pack,bi+con);
        con=con+pack;
    }
    for (int i=0;i<lenbi;i++){
    }
}

//Funciones para desencriptar

void uncodifie1(int pack,char* cfied){
    int lencfied=lenstr(cfied);
    int temp=0;
    int con=0;
    invtodo(pack,(cfied+con));
    temp=nor(pack,(cfied+con));
    con=con+pack;
    for (int i=1;i<(lencfied/pack);i++){
        if((pack/2==temp)&&(pack%2==0)){
            invtodo(pack,(cfied+con));
            temp=nor(pack,(cfied+con));
            con=con+pack;
        }
        else if (((pack/2>=temp)&&(pack%2==1))||(pack/2>temp)){
            invc2(pack,(cfied+con));
            temp=nor(pack,(cfied+con));
            con=con+pack;
        }
        else{
            invc3(pack,(cfied+con));
            temp=nor(pack,(cfied+con));
            con=con+pack;
        }
    }
}

void uncodifie2(int pack, char* cfied){
    int lencfied=lenstr(cfied);
    int con=0;
    for (int i=0;i<(lencfied/pack);i++){
        mvr(pack,cfied+con);
        con=con+pack;
    }
}

void mvr(int pack,char* cfied){
    char *temp;
    temp=new char [pack];
    for(int i=0;i<pack-1;i++){
        *(temp+i)=*(cfied+i+1);
    }
    *(temp+pack-1)=*(cfied);
    for(int i=0;i<pack;i++){
        *(cfied+i)=temp[i];
    }
    delete [] temp;
    return ;
}

//Funciones secundarias para encriptar

void invtodo(int pack,char* bi){
    for(int i=0;i<pack;i++){
        if (*(bi+i)=='0'){
            *(bi+i)='1';
        }
        else {
            *(bi+i)='0';
        }
    }
    return ;
}

void invc2(int pack,char* bi){
    for(int i=0;i<pack;i++){
        if ((i+1)%2==0){
            if (*(bi+i)=='0'){
                *(bi+i)='1';
            }
            else {
                *(bi+i)='0';
            }
        }
        else {
            if (*(bi+i)=='1'){
            }
        }
    }
    return ;
}

void invc3(int pack,char* bi){
    for(int i=0;i<pack;i++){
        if ((i+1)%3==0){
            if (*(bi+i)=='0'){
                *(bi+i)='1';
            }
            else {
                *(bi+i)='0';
            }
        }
        else {
            if (*(bi+i)=='1'){
            }
        }
    }
    return ;
}

void mv(int pack,char* bi){
    char *temp;
    temp=new char[pack];
    *temp=*(bi+pack-1);
    for(int i=1;i<pack;i++){
        *(temp+i)=*(bi-1+i);
    }
    for(int i=0;i<pack;i++){
        *(bi+i)=temp[i];
    }
    delete [] temp;
    return ;
}

//Funciones utiles

int lenstr(char* str){
    int con=0;
    while(*(str+con)!='\0'){
        con++;
    }
    return con;
}

int nor(int pack,char* str){
    int temp=0;
    for(int i=0;i<pack;i++){
        if(*(str+i)=='1'){
            temp++;
        }
    }
    return temp;
}



