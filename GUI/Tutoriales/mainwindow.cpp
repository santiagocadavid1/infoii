#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include "dialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->dial, SIGNAL(valueChanged(int)), ui->lcdNumber, SLOT(display(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_PushButton_clicked()
{
    ui->lcdNumber->display(0);
}

void MainWindow::on_actionCalender_triggered()
{
//    Dialog *MyDialog = new Dialog(); MyDialog->show();
}
