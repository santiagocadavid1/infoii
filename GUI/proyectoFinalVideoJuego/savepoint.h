#ifndef SAVEPOINT_H
#define SAVEPOINT_H
#include <QObject>
#include <QGraphicsItem>
#include <QPixmap>
#include <QPainter>


class SavePoint : public QObject,public QGraphicsItem
{
        Q_OBJECT
public:
    SavePoint();
    QRectF boundingRect() const; //Contorno del punto de guardado
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //pinta el punto de guardado
    QPixmap *pixmap;
    float x;
    float y;
};

#endif // SAVEPOINT_H
