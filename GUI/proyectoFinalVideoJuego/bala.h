#ifndef BALA_H
#define BALA_H
#include <QObject>
#include <QGraphicsItem>
#include <QPixmap>
#include <QPainter>
#include <QTimer>

class bala : public QObject,public QGraphicsItem
{
    Q_OBJECT
public:
    bala();
    //float x;
    //float y;
    bool dir;
    //bool be;
    QRectF boundingRect() const; //Contorno del personaje
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //pinta el personaje
    QTimer *timer3;              //Timer de la bala
    QPixmap *pixmap;
public slots:
    void mover();
};

#endif // BALA_H
