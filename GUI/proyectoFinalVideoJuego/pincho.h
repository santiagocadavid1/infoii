#ifndef PINCHO_H
#define PINCHO_H
#include <QObject>
#include <QGraphicsItem>
#include <QPixmap>
#include <QPainter>


class pincho : public QObject,public QGraphicsItem
{
        Q_OBJECT
public:
    pincho();
    QRectF boundingRect() const; //Contorno del punto de guardado
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //pinta el punto de guardado
    QPixmap *pixmap;
    float x;
    float y;
};

#endif // SAVEPOINT_H
