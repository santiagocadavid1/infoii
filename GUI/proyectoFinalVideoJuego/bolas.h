#ifndef BOLAS_H
#define BOLAS_H

#include <QPainter>
#include <QGraphicsItem>
#include <stdlib.h>
#include <math.h>
#include <QPixmap>
#include<QObject>
#include<QGraphicsScene>
#include<QDebug>

#define DT 0.1
#define G 10

class Bolas :public QGraphicsItem
{
public:
    double Px, Py, Vx, Vy, Ax, Ay;//datos de la esfera
    double Radio, Masa, Friccion, CoeficienteRestitucion;//propiedades fisicas
    bool NPiso;//determina si la esfera esta en el aire con cierta velocidad
    int rebotes;
public:
    Bolas();
    ~Bolas();//destructor

    //Funciones encargados de graficar
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);

    //Funciones de movimiento
    void CambioPosicion();
    void CambioVelocidad();
    void Aceleracion();
    void ColisionX();
    void ColisionY(bool);
    bool especial=false;
    bool mata=true;
    QPixmap *pixmap;

    QPainter *painter;

};

#endif // BOLAS_H
