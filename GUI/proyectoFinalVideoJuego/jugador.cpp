#include "jugador.h"
#include <QDebug>
#include <QGraphicsScene>
jugador::jugador(QObject *parent) : QObject(parent)
{
    filas = 0;
    columnas = 0;
    pixmap = new QPixmap(":/imagenes/Personaje.png");
    x=75;
    y=500;
    //dimensiones de c/U de las imagenes
    ancho = 100;
    alto  = 100;
    //velocidad inicial
    vy=0;
    vx=0;
    sp[0]=75;   //Posicion de guardado por defecto en x
    sp[1]=500;  //Posicion de guardado por defecto en y
}
//arduino
/*void jugador::posicion(int v_limit)
{
    setPos(x, v_limit-y-50);
}*/


QRectF jugador::boundingRect() const
{
    return QRectF(-ancho/2,-alto/2,ancho,alto);
}

void jugador::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawPixmap(-ancho/2,-alto/2,*pixmap,columnas,filas,ancho,alto);
}

/*void jugador::keyPressEvent(QKeyEvent *evento)
{
    //qDebug()<<"parce funcine";
    a=0;
    if(evento->key()==Qt::Key_Right){
            qDebug()<<"derecha";
            if (vx<7&&x<950){
                vx=vx+4;
            }
            movimiento();
            /*x+=7;
            setPos(x,y);
            filas=100;
            columnas+=100;
            if(columnas>=600){
                columnas=0;
            }
    }
    if(evento->key()==Qt::Key_Left){
        qDebug()<<"izquierda";
        if (vx>-7&&x>100){
            vx=vx-4;
        }
        movimiento();
        /*x-=7;
        setPos(x,y);
        filas=300;
        columnas+=100;
        if(columnas>=600){
            columnas=0;
        }
    }
    if(evento->key()==Qt::Key_Up){
        qDebug()<<"arriba";
        if(y>=100&&(djump<2)){
            vy=-16;
        }
        djump+=1;
        caida();
        filas=0;
        columnas+=100;
        if(columnas>=600){
            columnas=0;
        }
    }

    if(evento->key()==Qt::Key_Down){
        qDebug()<<"abajo";
        m45 = new bala();
        scene()->addItem(m45);


        }

}*/
//Cuando el personaje no esta en el piso
void jugador::caida(){
    y+=vy;
    x+=vx;
    setPos(x,y);
    vy=vy+g;
    /*if(vx>0){
        vx=vx-ff+0.38;
    }
    else if(vx<0){
        vx=vx+ff-0.38;
    }*/
}
//Cuando el personaje llega o esta en el piso
void jugador::choque(){
    vy=0;
    djump=0;
    if(vx>0){
        vx=vx-ff;
    }
    else if(vx<0){
        vx=vx+ff;
    }
}
// movimiento en x del personaje
void jugador::movimiento(){
    x+=vx;
    setPos(x,y);
}

void jugador::muerte(){
    x=sp[0];
    y=sp[1];
    setPos(x,y);
}


