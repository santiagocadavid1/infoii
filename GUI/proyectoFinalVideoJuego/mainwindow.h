#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <jugador.h>
#include <bala.h>
#include <trap.h>
#include <pincho.h>
#include <savepoint.h>
#include "bolas.h"
#include <QKeyEvent>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QFileDialog>
#include <QDebug>
#include <QTextStream>
//Widgets
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <fstream>
#include <cstdlib>
#include <string>
#include <QProgressBar>
#include <QSplashScreen>
#include <QLCDNumber>
using namespace std;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void animar();  //Funcion para movimiento en la escena
    void crearbolas();
    void keyPressEvent(QKeyEvent *evento);
private slots:
    void on_actionConnect_triggered(); //Funcion para conectar el arduino
    void Joy();                        //Funcion para recivir señales del arduino
    void MenuLogin();                  //Carga la escena de login
    void MenuRegistro();               //Carga la escena de registro
    void Start();                      //Start temporal
    void RegistroS();                   //Toma de datos de registro
    void LoginS();                      //Toma de datos de login
    void BackToMenu();                  //Volver al menu principalj
    void MultiPlayer();                       //MultiJugador
    void MultiPlayerV(char v);
    void tiempo();
    void victoria();



private:
    Ui::MainWindow *ui;                 // el view de todo el juego
    QGraphicsScene *etapa1;             // escena del nivel 1 del juego
    QGraphicsScene *etapa2;             // escena del nivel 2 del juego
    QGraphicsScene *multip;             // escena del multijugador
    QGraphicsScene *menu;               // scene del menu
    QGraphicsScene *Login;              // escena del login
    QGraphicsScene *Registro;           // escena del registro
    jugador *Trump;                     //Personaje principal
    jugador *Obama;                     //Multiplayer

    QGraphicsLineItem *lsbarra1;
    QGraphicsLineItem *libarra1;
    QGraphicsLineItem *lizbarra1;
    QGraphicsLineItem *ldbarra1;

    QGraphicsLineItem *lsbarra2;
    QGraphicsLineItem *libarra2;
    QGraphicsLineItem *lizbarra2;
    QGraphicsLineItem *ldbarra2;

    QGraphicsLineItem *lsbarra3;
    QGraphicsLineItem *libarra3;
    QGraphicsLineItem *lizbarra3;
    QGraphicsLineItem *ldbarra3;

    QGraphicsLineItem *lsbarra4;
    QGraphicsLineItem *libarra4;
    QGraphicsLineItem *lizbarra4;
    QGraphicsLineItem *ldbarra4;

    //Balas
    //bala    *bala1;

    QList<bala*>    balas;              //Lista de balas
//    QList<bala*>    balas2;              //Lista de balas obama
    //Trampa
    Trap    *trampa;
    Trap    *trampa1;
    Trap    *trampa2;
    Trap    *trampa3;
    //Pinchos
    QList<pincho*> pinchos;
    //SavePoint
    SavePoint *SP;
    //Bolas nivel 2

    QList<Bolas*> bolas;
    QTimer *Tiempo;

    QTimer *timer_control;              //Timer para control por arduino
    QSerialPort *control;
    //margenes
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;//trazado del juego

    QTimer *timer;//Timer animar

    //Botones menu
    QPushButton *login, *registro;
    QPushButton *start;     //Temporal
    QPushButton *MainMenu;  //Volver al menu principal
    QPushButton *Multi;     //Boton para el modo multijugador

    //Botones Log in
    QLabel *UN,*PW,*CPW;    //Username, password, password confirm
    QLineEdit *un,*pw,*cpw;
    string checkUN;
    string aux="";

    // barra de vida
    QProgressBar *vida1;
    QProgressBar *vida2;

    //Registro
    bool sameUser;          //compara si el usuario ingresado ya existe en el menu registro
    int  checkUser;         //entero que va contando que lineas tiene que comparar
    QLabel *warnPa,*warnUs;// encargado de avisar cuando hay un usuario incorrecto, encargado de avisar cuando hay una contraseña incorrecta
    int nivel=1;
    //nivel = 7 multijugador
    QString auxuser;

    // timer del splash multijugador
    QTimer *timer_splash;
    QSplashScreen *ganador;
    QSplashScreen *victoriacampal;
    QTimer *ganehpta;
    //lcd
    QLCDNumber *lcd;
    QTimer  *clock;

    int auxtime=30;

    float auxpos[2]={};
    int sis=0;

};

#endif // MAINWINDOW_H
