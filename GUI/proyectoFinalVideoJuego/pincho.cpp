#include "pincho.h"

pincho::pincho()
{
    pixmap= new QPixmap(":/imagenes/pincho.png");
}

QRectF pincho::boundingRect() const
{
    return QRectF(-60,-22.5,120,45);
}

void pincho::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->drawPixmap(-60,-22.5,*pixmap,0,0,120,45);
}

