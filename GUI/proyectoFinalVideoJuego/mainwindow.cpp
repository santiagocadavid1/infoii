#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsScene>
#include <QFile>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    auxpos[0]=75;
    auxpos[1]=500;
    ui->setupUi(this);

    //Escena menu
    menu=new QGraphicsScene(0,0,1080,700);

    ui->graphicsView->setScene(menu);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->adjustSize();
    ui->graphicsView->show();
    ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/imagenes/FondoT.png")));

    //Boton login
    login=new QPushButton();
    menu->addWidget(login);
    login->setGeometry(465,300,150,50);
    login->setText("Log in");
    login->setFont(QFont("Arial"));
    //Boton registro
    registro=new QPushButton();
    menu->addWidget(registro);
    registro->setGeometry(465,400,150,50);
    registro->setText("Registro");
    registro->setFont(QFont("Arial"));

    //Boton start temporal
    start=new QPushButton();
    menu->addWidget(start);
    start->setGeometry(465,500,150,50);
    start->setText("Guest");
    start->setFont(QFont("Arial"));

    //Boton multiplayer
    Multi=new QPushButton();
    menu->addWidget(Multi);
    Multi->setGeometry(465,600,150,50);
    Multi->setText("MultiPlayer");
    Multi->setFont(QFont("Arial"));

    connect(login,SIGNAL(released()),this,SLOT(MenuLogin()));       //Conecta el boton de login con el menu de login
    connect(registro,SIGNAL(released()),this,SLOT(MenuRegistro())); //Conecta el boton de registro con el menu de registro
    connect(start,SIGNAL(released()),this,SLOT(Start()));           //Conecta el boton de guest con el juego
    connect(Multi,SIGNAL(released()),this,SLOT(MultiPlayer()));

}

void MainWindow::animar()                                           //Metodo para animar
{
    //
    if(nivel==1){
        qDebug()<<"aimar nivel 1";
        trampa->caida();
        if (Trump->collidesWithItem(l3)||Trump->y>=627){                //Colide de trump con el piso
                Trump->choque();

        }
        else{                                                           //Caida de trump
            Trump->caida();
        }
        if (Trump->collidesWithItem(l2)||Trump->collidesWithItem(l4)){  //Colide de trump con los muros
            Trump->vx=0;
        }


        for(int i=0; i<balas.length();i++)                              //Controla los collides de cada bala
        {
            balas.at(i)->mover();                                       //Controla el movimiento de las balas
            if(balas.at(i)->collidesWithItem(SP)){                      //Choque de las balas con el check point
                Trump->sp[0]=Trump->x;
                Trump->sp[1]=Trump->y;
                //qDebug()<<"Save";
                QFile inputFile("posiciones.txt");
                sameUser=false;
                checkUser=0;
                QString cadena;
                int s=0;
                if (inputFile.open(QIODevice::ReadOnly))//abre file en modo lectura    y hace lo mismo que arriba
                {
                    QTextStream in(&inputFile);         //capta lo que hay en file
                    while (!in.atEnd())
                    {
                      QString line = in.readLine();     //lee linea por linea lo que hay en el archivo
                      qDebug()<<line;
                      qDebug()<<"este es el usuario"<<auxuser;
                      checkUser++;
                      if(sameUser && s<=2){
                          qDebug()<<"entre en el cambio";
                          if(s==0){
                              qDebug()<<QString::number(nivel);
                              cadena+=QString::number(nivel);
                              cadena+="\r\n";
                          }
                          if(s==1){
                              cadena+=QString::number(int(Trump->sp[0]));
                              cadena+="\r\n";
                          }
                          if(s==2){
                              cadena+=QString::number(int(Trump->sp[1]));
                              cadena+="\r\n";
                              sameUser=false;
                          }
                          s++;
                      }
                      else cadena+=line+"\r"+"\n";
                      if(auxuser==line){
                          sameUser=true;
                          qDebug()<<"true";
                      }



                    }
                    inputFile.close();
                    qDebug()<<cadena;
                    QFile out("posiciones.txt");
                    QTextStream ofile(&out);
                    out.open(QIODevice::WriteOnly);
                    ofile<<cadena;
                    out.close();
                    //qDebug()<<Trump->x;
                    //qDebug()<<Trump->y;
                }
            }
            if(!balas.at(i)->collidingItems().empty())                  //Collide de la bala
            {
                if(!balas.at(i)->collidesWithItem(Trump)){              //La bala no choca con trump

                    etapa1->removeItem(balas.at(i));                    //Quita la bala
                    //balas.at(i)->choque();
                }
            }
        }

        if (Trump->collidesWithItem(trampa)){                           //Colide de trump con trampas
            Trump->muerte();
            qDebug()<<"Trump murio";

        }
        for(int i=0; i<pinchos.length();i++){
            if(Trump->collidesWithItem(pinchos.at(i))){                     // trump muere por los pinchos
                Trump->muerte();
                qDebug()<<"Trump murio";
            }
        }
        //Choque con plataformas // limite del eje y 40 mas arriba del collider
        if((Trump->collidesWithItem(lsbarra1)&&Trump->y<=465)||(Trump->collidesWithItem(lsbarra2)&&Trump->y<=465)||(Trump->collidesWithItem(lsbarra3)&&Trump->y<=325)||(Trump->collidesWithItem(lsbarra4)&&Trump->y<=223)){
            if(Trump->vx>0){
                Trump->vx=Trump->vx-ff;
            }
            else if(Trump->vx<0){
                Trump->vx=Trump->vx+ff;
            }
            if(Trump->vy<0){
                Trump->caida();
            }
            else{
                Trump->choque();
            }
        }
        //Pasar al nivel 2
        if(Trump->x>=970&&Trump->x<=1025&&Trump->y<=253&&Trump->y>=150){
            nivel=2;
            Trump->sp[0]=75;
            Trump->sp[1]=500;
            qDebug()<<Trump->x;
            qDebug()<<Trump->y;
            QFile inputFile("posiciones.txt");
            sameUser=false;
            checkUser=0;
            QString cadena;// string que alacena todo lo que se va a reescribir en el txt
            int s=0;
            if (inputFile.open(QIODevice::ReadOnly))//abre file en modo lectura
            {
                QTextStream in(&inputFile);         //capta lo que hay en file
                while (!in.atEnd())
                {
                  QString line = in.readLine();     //lee linea por linea lo que hay en el archivo
                  qDebug()<<line;
                  checkUser++;
                  if(sameUser && s<=2){                       // entra cuando encontro el usuario y aqui modifica los valores
                      if(s==0){
                          qDebug()<<QString::number(nivel);
                          cadena+=QString::number(2);   // nivel 2
                          cadena+="\r\n";
                      }
                      if(s==1){
                          cadena+=QString::number(75);            // posicion en x
                          cadena+="\r\n";
                      }
                      if(s==2){
                          cadena+=QString::number(500);                 // posicion en y
                          cadena+="\r\n";
                          sameUser=false;
                      }
                      s++;
                  }
                  else cadena+=line+"\r"+"\n";
                  if(auxuser==line){                                    //encuentra el usuario
                      sameUser=true;
                  }



                }
                inputFile.close();
                qDebug()<<cadena;
                QFile out("posiciones.txt");
                QTextStream ofile(&out);
                out.open(QIODevice::WriteOnly);
                ofile<<cadena;                                           // escirbe todo con los nuevos datos
                out.close();
           timer->stop();
            Start();
        }
        qDebug()<<"Hola";
    }
    }
    //Nivel 2
    if(nivel==2){
        trampa1->caida();
        trampa2->caida();
        trampa3->caida();
        //trampa->caida();
        for (int i=0; i<bolas.length(); i++){
            if (bolas.at(i)->collidesWithItem(l3)){
                //escenario->removeItem(Esferascaida.at(i));
                //Esferascaida.at(i)->setPos(5000,-39);
                bolas.at(i)->ColisionY(true);
                bolas.at(i)->rebotes++;
                if(bolas.at(i)->rebotes==5){
                    bolas.at(i)->mata=false;
                    bolas.at(i)->setPos(0,0);
                    etapa2->removeItem(bolas.at(i));
                }
            }
            if(bolas.at(i)->collidesWithItem(l1)){
                bolas.at(i)->ColisionY(false);
            }
            if(bolas.at(i)->collidesWithItem(l2) || bolas.at(i)->collidesWithItem(l4)){
                bolas.at(i)->ColisionX();
                bolas.at(i)->rebotes++;
                if(bolas.at(i)->rebotes==5){
                    bolas.at(i)->mata=false;
                    bolas.at(i)->setPos(0,0);
                    etapa2->removeItem(bolas.at(i));

                }
            }
            if(bolas.at(i)->x()-Trump->x<=20&&bolas.at(i)->x()-Trump->x>=-20&&bolas.at(i)->y()-Trump->y<=20&&bolas.at(i)->y()-Trump->y>=-20&&bolas.at(i)->mata){
                bolas.at(i)->setPos(0,0);
                etapa2->removeItem(bolas.at(i));
                //Trump->vida-=10;
                timer->stop();
                Tiempo->stop();
                clock->stop();
                trampa1->timer4->stop();
                trampa2->timer4->stop();
                trampa3->timer4->stop();
                auxtime=30;
                Start();
            }

            bolas.at(i)->Aceleracion();
            bolas.at(i)->CambioVelocidad();
            bolas.at(i)->CambioPosicion();
            bolas.at(i)->setPos(bolas.at(i)->Px,591-bolas.at(i)->Py);
        }
        if (trampa1->collidesWithItem(Trump)){
            //etapa2->removeItem(trampa1);
            //Trump->vida-=33;
            timer->stop();
            Tiempo->stop();
            clock->stop();
            trampa1->timer4->stop();
            trampa2->timer4->stop();
            trampa3->timer4->stop();
            auxtime=30;
            Start();
        }
        if (trampa2->collidesWithItem(Trump)){
            //etapa2->removeItem(trampa2);
            //Trump->vida-=33;
            timer->stop();
            Tiempo->stop();
            clock->stop();
            trampa1->timer4->stop();
            trampa2->timer4->stop();
            trampa3->timer4->stop();
            auxtime=30;
            Start();
        }
        if (trampa3->collidesWithItem(Trump)){
            //etapa2->removeItem(trampa3);
            //Trump->vida-=33;
            timer->stop();
            Tiempo->stop();
            clock->stop();
            trampa1->timer4->stop();
            trampa2->timer4->stop();
            trampa3->timer4->stop();
            auxtime=30;
            Start();
        }
        qDebug()<<Trump->vida;

        if (Trump->collidesWithItem(l3)||Trump->y>=644){                //Colide de trump con el piso
                Trump->choque();

        }
        else{                                                           //Caida de trump
            Trump->caida();
        }
        if (Trump->collidesWithItem(l2)||Trump->collidesWithItem(l4)){  //Colide de trump con los muros
            Trump->vx=0;
        }
        for(int i=0; i<balas.length();i++)                              //Controla los collides de cada bala
        {
            balas.at(i)->mover();                                       //Controla el movimiento de las balas

        }
        if(auxtime==0){
            timer->stop();
            Tiempo->stop();
            clock->stop();
            trampa1->timer4->stop();
            trampa2->timer4->stop();
            trampa3->timer4->stop();
            victoria();
        }

    }

    //Animar del multijugador
    if(nivel==7){
        if (Obama->collidesWithItem(l3)||Obama->y>=644){                //Colide de trump con el piso
                Obama->choque();

        }
        else{                                                           //Caida de trump
            Obama->caida();
        }
        if (Obama->collidesWithItem(l2)||Obama->collidesWithItem(l4)){  //Colide de trump con los muros
            Obama->vx=0;
        }
        /*///////////////////*/
        if (Trump->collidesWithItem(l3)||Trump->y>=644){                //Colide de trump con el piso
                Trump->choque();

        }
        else{                                                           //Caida de trump
            Trump->caida();
        }
        if (Trump->collidesWithItem(l2)||Trump->collidesWithItem(l4)){  //Colide de trump con los muros
            Trump->vx=0;
        }
        /*///////////////////*/
        for (int i=0; i<balas.length();i++){
            if(!balas.at(i)->collidingItems().empty())                  //Collide de la bala
            {
                if(!balas.at(i)->collidesWithItem(Trump)||!balas.at(i)->collidesWithItem(Obama)){              //La bala no choca con trump
                    if (balas.at(i)->collidesWithItem(Obama)&&balas.at(i)->ws=='T'){                            //La bala le pega a Obama
                        Obama->vida-=5;                                             //Pierde vida
                        qDebug()<<Obama->vida;
                        vida2->setValue(Obama->vida);                               //Cambio en la barra de progreso
                    }
                    if (balas.at(i)->collidesWithItem(Trump)&&balas.at(i)->ws=='O'){                            //La bala le pega a Trump
                        Trump->vida-=5;                                             //Pierde vida
                        qDebug()<<Trump->vida;
                        vida1->setValue(Trump->vida);                               //Cambio en la barra de progreso
                    }
                    multip->removeItem(balas.at(i));                    //Quita la bala
                }

            }
        }
        if (Trump->vida==0){        //Gana Obama, carga el splash
            qDebug()<<"gano Obama";
            MultiPlayerV('O');
        }
        if (Obama->vida==0){        //Gana Trump, carga el splash
            qDebug()<<"gano Trump";
            MultiPlayerV('T');
        }

    }

}

void MainWindow::on_actionConnect_triggered() //Connect del arduino
{
    control->setPortName("COM5");
    //timer_control->start(100);
    if(control->open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!control->setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<control->errorString();

        if(!control->setDataBits(QSerialPort::Data8))
            qDebug()<<control->errorString();

        if(!control->setParity(QSerialPort::NoParity))
            qDebug()<<control->errorString();

        if(!control->setStopBits(QSerialPort::OneStop))
            qDebug()<<control->errorString();

        if(!control->setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<control->errorString();
        timer->stop();                  //se para el timer de animar
            timer_control->start(20);   //Se inicia el timer de arduino
    }
    else{
        qDebug()<<"Serial COM3 not opened. Error: "<<control->errorString();
    }
}                                       // Configuracion default
void MainWindow::Joy(){                 // Toma de datos del arduino
        char data;
        int l = 0;
        bool flag=true;
        //animar
        animar();

        /*

        if (Trump->collidesWithItem(l3)){
            Trump->choque();
        }
        else{
            Trump->caida();
        }

        */
           if(control->waitForReadyRead(100)){

                //Data was returned
                l = control->read(&data,1);
                switch (data) {
                case 'A':                               //Movimiento hacia la izquierda
                    qDebug()<<"izquierda";
                    if (Trump->vx>-6&&Trump->x>100){
                        Trump->vx-=4;
                    }
                    Trump->movimiento();

                    /*Trump->x=Trump->x-10;
                    Trump->setPos(Trump->x,Trump->y);*/

                    Trump->filas=141.25;
                    Trump->columnas+=121;
                    if(Trump->columnas>484){
                        Trump->columnas=0;
                    }

                    Trump->dir=0;
                    break;
                case 'B':                               //Movimento hacia la derecha
                    qDebug()<<"derecha";
                    if (Trump->vx<6&&Trump->x<950){
                        Trump->vx+=4;
                    }
                    Trump->movimiento();

                    /*Trump->x=Trump->x+10;
                    Trump->setPos(Trump->x,Trump->y);*/

                    Trump->filas=0;
                    Trump->columnas+=121;
                    if(Trump->columnas>484){
                        Trump->columnas=0;
                    }
                    Trump->dir=1;
                    break;
                case 'C':                               //Movimiento hacia arriba
                    qDebug()<<"arriba";
                    if(Trump->y>=100&&(Trump->djump<2)){
                        Trump->vy=-9;
                    }
                    Trump->djump+=1;
                    Trump->caida();

                    /*Trump->vy=Trump->vy-30;
                    //Trump->setPos(Trump->x,Trump->y);
                    Trump->caida();*/
                    if(Trump->dir==0){
                        Trump->filas=141.25;
                        Trump->columnas+=121;

                    }
                    else{
                        Trump->filas=0;
                        Trump->columnas+=121;

                    }

                    if(Trump->columnas>484){
                        Trump->columnas=0;
                    }
                    break;
                case 'D':                               //Disparo

                    balas.append(new bala());                           //Agrega una bala a la lista de balas
                    etapa1->addItem(balas.last());                      //Agrega la bala a la escena
                    //balas.last()->setPos(->x(),ev->y());
                    if (Trump->dir){                                    //Le da la direccion a la bala
                        balas.last()->setPos(Trump->x+57,Trump->y-10);
                    }
                    else{
                        balas.last()->setPos(Trump->x-57,Trump->y-10);
                    }
                    balas.last()->dir=Trump->dir;


                    //bee=1;

                    /*bala1=new bala();
                    bala1->setPos(Trump->x,Trump->y);
                    etapa1->addItem(bala1);
                    bala1->dir=Trump->dir;*/

                    /*
                    Trump->y=Trump->y+10;
                    Trump->setPos(Trump->x,Trump->y);*/

                    Trump->filas=200;
                    Trump->columnas+=100;
                    if(Trump->columnas>=600){
                        Trump->columnas=0;
                    }
                    break;
                case 'K':
                    timer_control->stop();
                    control->close();
                    //close();
                    break;
                case 'N':
                    break;
                default:
                    break;
                }
                //Trump->posicion(400);
                qDebug()<<"Response: "<<data;
                flag=false;

            }else{
                //No data
                qDebug()<<"Time out";
          }
        //}
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //control->close();

}

void MainWindow::MenuLogin()
{
    //Escena menu
    Login=new QGraphicsScene(0,0,1080,700);
    qDebug()<<"estoy entrando";
    ui->graphicsView->setScene(Login);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->adjustSize();
    ui->graphicsView->show();
    ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/imagenes/FondoT.png")));

    //Botones UN
    UN=new QLabel();
    Login->addWidget(UN);                       //Agrega el label de user name
    UN->setGeometry(265,300,150,50);            //Le da el tamaño y la posicion al label de user name
    UN->setText("      User name");
    UN->setFont(QFont("Arial"));
    un=new QLineEdit();
    Login->addWidget(un);                      //Agrega el line edit de user name
    un->setPlaceholderText("Usuario");
    un->setMaxLength(20);

    un->setGeometry(415,300,250,50);          //Le da el tamaño y la posicion a el line edit de user name

    //Botones PW
    PW=new QLabel();                          //Agrega el label de password
    Login->addWidget(PW);
    PW->setGeometry(265,400,150,50);          //Le da el tamaño y la posicion al label de password
    PW->setText("      Password");
    PW->setFont(QFont("Arial"));
    pw=new QLineEdit();                       //Agrega el line edit de password
    Login->addWidget(pw);
    pw->setPlaceholderText("Contraseña");
    pw->setMaxLength(20);
    pw->setGeometry(415,400,250,50);         //Le da el tamaño y la posicio al line edit de password

    pw->setEchoMode(QLineEdit::Password);// oculta el texto

    //Boton guest
    start=new QPushButton();
    Login->addWidget(start);
    start->setGeometry(465,500,150,50);
    start->setText("Start");
    start->setFont(QFont("Arial"));
    connect(start,SIGNAL(released()),this,SLOT(LoginS()));      //Conecta el boton de start del menu de login con la confirmacion de login

    //Boton back main menu

    MainMenu=new QPushButton();
    Login->addWidget(MainMenu);
    MainMenu->setGeometry(50,50,150,50);
    MainMenu->setText("Back to Main Menu");
    MainMenu->setFont(QFont("Arial"));

    connect(MainMenu,SIGNAL(released()),this,SLOT(BackToMenu()));   //Conecta el boton back to menu con el menu principal

}

void MainWindow::MenuRegistro()
{
    //Escena menu
    Registro=new QGraphicsScene(0,0,1080,700);
    qDebug()<<"estoy entrando";
    ui->graphicsView->setScene(Registro);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->adjustSize();
    ui->graphicsView->show();
    ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/imagenes/FondoT.png")));

    //Botones UN
    UN=new QLabel();
    Registro->addWidget(UN);
    UN->setGeometry(265,300,150,50);
    UN->setText("      User name");
    UN->setFont(QFont("Arial"));
    un=new QLineEdit();
    Registro->addWidget(un);
    un->setPlaceholderText("Usuario");
    un->setMaxLength(20);
/*  QString user;
    user=un->text();                         //retorna qstring con lo que haya en el cuadro.
    qDebug()<<user;*/
    un->setGeometry(415,300,250,50);

    //Botones PW
    PW=new QLabel();
    Registro->addWidget(PW);
    PW->setGeometry(265,400,150,50);
    PW->setText("      Password");
    PW->setFont(QFont("Arial"));
    pw=new QLineEdit();
    Registro->addWidget(pw);
    pw->setPlaceholderText("Contraseña");
    pw->setMaxLength(20);
    pw->setGeometry(415,400,250,50);

    pw->setEchoMode(QLineEdit::Password);// oculta el texto

    //Botones PWC
    CPW=new QLabel();
    Registro->addWidget(CPW);
    CPW->setGeometry(265,500,150,50);
    CPW->setText("      Password");
    CPW->setFont(QFont("Arial"));
    cpw=new QLineEdit();
    Registro->addWidget(cpw);
    cpw->setPlaceholderText("Confirmacion de Contraseña");
    cpw->setMaxLength(20);
    cpw->setGeometry(415,500,250,50);

    cpw->setEchoMode(QLineEdit::Password);// oculta el texto

    //Boton start temporal
    start=new QPushButton();
    Registro->addWidget(start);
    start->setGeometry(465,600,150,50);
    start->setText("Start");
    start->setFont(QFont("Arial"));
    connect(start,SIGNAL(released()),this,SLOT(RegistroS()));       //Conecta el boton de start de registro con la confirmacion de registro

    //Boton main menu

    MainMenu=new QPushButton();
    Registro->addWidget(MainMenu);
    MainMenu->setGeometry(50,50,150,50);
    MainMenu->setText("Back to Main Menu");
    MainMenu->setFont(QFont("Arial"));

    connect(MainMenu,SIGNAL(released()),this,SLOT(BackToMenu()));   //Conecta el boton back to menu con el menu principal

}

void MainWindow::Start()
{
    if (nivel==1){
        etapa1= new QGraphicsScene(this);

        etapa1->setSceneRect(0,0,1080,680);
        Trump=new jugador();
        trampa=new Trap();
        SP=new SavePoint();

        timer_control=new QTimer;           //Timer para el puerto serial
        control = new QSerialPort(this);

        connect(timer_control,SIGNAL(timeout()),this,SLOT(Joy()));

        timer=new QTimer();
        connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
        timer->start(10);

        //bala1=new bala();
        etapa1->addItem(Trump);
        //etapa1->addItem(bala1);

        Trump->sp[0]=auxpos[0];
        Trump->sp[1]=auxpos[1];
        Trump->x=Trump->sp[0];
        Trump->y=Trump->sp[1];

        //qDebug()<<"se añadio una bala";

        Trump->setFlag(QGraphicsItem::ItemIsFocusable);
        Trump->setFocus();
        ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui->graphicsView->setScene(etapa1);
        ui->graphicsView->adjustSize();
        ui->graphicsView->show();
        //margenes
        l1=new QGraphicsLineItem(50,57,1025,57);
        l2=new QGraphicsLineItem(50,57,50,627);
        l3=new QGraphicsLineItem(50,627,1025,627);
        l4=new QGraphicsLineItem(1025,57,1025,627);
        etapa1->addItem(l1);//barra horizontal de arriba
        etapa1->addItem(l2);//barra vertical de la izquierda
        etapa1->addItem(l3);//barra horizontal de abajo
        etapa1->addItem(l4);//barra vertical de la derecha
        etapa1->addItem(trampa);//trampa
        etapa1->addItem(SP);//check point
        trampa->ymax=455;
        trampa->setPos(trampa->x,trampa->y);
        SP->setPos(SP->x,SP->y);

        //Boton main menu

        MainMenu=new QPushButton();
        etapa1->addWidget(MainMenu);
        MainMenu->setGeometry(0,0,150,50);
        MainMenu->setText("Back to Main Menu");
        MainMenu->setFont(QFont("Arial"));
        MainMenu->setStyleSheet("QPushButton { background-color : transparent;color : black;}");

        connect(MainMenu,SIGNAL(released()),this,SLOT(BackToMenu()));   //conecta el boton de back to menu al menu principal

        //Plataformas
        lsbarra1=new QGraphicsLineItem(222,495,287,495);
        etapa1->addItem(lsbarra1);
        libarra1=new QGraphicsLineItem(192,515,317,515);
        etapa1->addItem(libarra1);
        lizbarra1=new QGraphicsLineItem(192,495,192,515);
        etapa1->addItem(lizbarra1);
        ldbarra1=new QGraphicsLineItem(317,495,317,515);
        etapa1->addItem(ldbarra1);
        //Collider 30 mas pequeño en ambos lados
        lsbarra2=new QGraphicsLineItem(592,495,685,495);
        etapa1->addItem(lsbarra2);
        libarra2=new QGraphicsLineItem(562,517,715,517);
        etapa1->addItem(libarra2);
        lizbarra2=new QGraphicsLineItem(562,495,562,517);
        etapa1->addItem(lizbarra2);
        ldbarra2=new QGraphicsLineItem(715,495,715,517);
        etapa1->addItem(ldbarra2);

        lsbarra3=new QGraphicsLineItem(363,355,483,355);
        etapa1->addItem(lsbarra3);
        libarra3=new QGraphicsLineItem(333,377,513,377);
        etapa1->addItem(libarra3);
        lizbarra3=new QGraphicsLineItem(333,355,333,377);
        etapa1->addItem(lizbarra3);
        ldbarra3=new QGraphicsLineItem(513,355,513,377);
        etapa1->addItem(ldbarra3);

        lsbarra4=new QGraphicsLineItem(948,253,1025,253);
        etapa1->addItem(lsbarra4);
        libarra4=new QGraphicsLineItem(918,276,1025,276);
        etapa1->addItem(libarra4);
        lizbarra4=new QGraphicsLineItem(918,253,918,276);
        etapa1->addItem(lizbarra4);
        ldbarra4=new QGraphicsLineItem(1025,253,1025,276);
        etapa1->addItem(ldbarra4);

        //barra=new QGraphicsRectItem(-60,-10,120,20);
        //etapa1->addItem(barra);
        //barra->setPos(250,500);
        //Fondo
        ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/imagenes/Fondo.png")));

        //Pinchos
        pinchos.append(new pincho());
        etapa1->addItem(pinchos.last());
        pinchos.last()->setPos(254,605);
        pinchos.append(new pincho());
        etapa1->addItem(pinchos.last());
        pinchos.last()->setPos(845,605);
        pinchos.append(new pincho());
        etapa1->addItem(pinchos.last());
        pinchos.last()->setPos(965,605);
    }
    else if (nivel==2){
        qDebug()<<" ENTRE AL NIVEL 2 ...........";
        etapa2= new QGraphicsScene(this);

        etapa2->setSceneRect(0,0,1080,680);
        Trump=new jugador();
        Trump->sp[0]=auxpos[0];
        Trump->sp[1]=auxpos[1];
        Trump->x=Trump->sp[0];
        Trump->y=Trump->sp[1];
        qDebug()<<Trump->x;
        qDebug()<<Trump->y;

        Trump->setPos(x(),y());
        etapa2->addItem(Trump);
        timer_control=new QTimer;           //Timer para el puerto serial
        control = new QSerialPort(this);

        connect(timer_control,SIGNAL(timeout()),this,SLOT(Joy()));

        Tiempo=new QTimer;
        connect(Tiempo,SIGNAL(timeout()),SLOT(crearbolas()));
        Tiempo->start(3500);
        timer=new QTimer();
        connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
        timer->start(10);

        lcd=new QLCDNumber;
        lcd->setGeometry(465,100,100,100);
        lcd->setStyleSheet("QLCDNumber { background-color : transparent;color : red;}");
        etapa2->addWidget(lcd);

        clock=new QTimer();
        connect(clock,SIGNAL(timeout()),SLOT(tiempo()));
        clock->start(1000);

        //trampas
        trampa1=new Trap();
        trampa1->y+=150;
        trampa1->ymax=570;
        trampa1->setPos(trampa1->x,trampa1->y);
        etapa2->addItem(trampa1);//trampa
        trampa2=new Trap();
        trampa2->x+=550;
        trampa2->y+=150;
        trampa2->ymax=570;
        trampa2->setPos(trampa2->x,trampa2->y);
        etapa2->addItem(trampa2);//trampa

        trampa3=new Trap();
        trampa3->x+=280;
        trampa3->y+=150;
        trampa3->ymax=570;
        trampa3->setPos(trampa3->x,trampa3->y);
        etapa2->addItem(trampa3);//trampa





        //Trump->setFlag(QGraphicsItem::ItemIsFocusable);
        //Trump->setFocus();
        ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui->graphicsView->setScene(etapa2);
        ui->graphicsView->adjustSize();
        ui->graphicsView->show();
        //margenes
        l1=new QGraphicsLineItem(50,40,1025,40);
        l2=new QGraphicsLineItem(50,40,50,644);
        l3=new QGraphicsLineItem(50,644,1025,644);
        l4=new QGraphicsLineItem(1025,40,1025,644);
        etapa2->addItem(l1);//barra horizontal de arriba
        etapa2->addItem(l2);//barra vertical de la izquierda
        etapa2->addItem(l3);//barra horizontal de abajo
        etapa2->addItem(l4);//barra vertical de la derecha
        ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/imagenes/Fondo2.png")));

        //Boton main menu

        /*MainMenu=new QPushButton();
        etapa2->addWidget(MainMenu);
        MainMenu->setGeometry(0,0,150,50);
        MainMenu->setText("Back to Main Menu");
        MainMenu->setFont(QFont("Arial"));
        MainMenu->setStyleSheet("QPushButton { background-color : transparent;color : black;}");

        connect(MainMenu,SIGNAL(released()),this,SLOT(BackToMenu()));*/   //conecta el boton de back to menu al menu principal
        qDebug()<<"oe";

    }

}

void MainWindow::RegistroS()
{
    QString user;
    user=un->text();                         //capta lo escrito en user name
    QString Pass;
    Pass=pw->text();                        //capta lo escrito en contraseña
    QString Pass2;
    Pass2=cpw->text();                      //capta lo escrito en confirmacion
    QFile inputFile("file.txt");
    sameUser=false;
    checkUser=0;
    if (inputFile.open(QIODevice::ReadOnly))//abre file en modo lectura
    {
        QTextStream in(&inputFile);         //capta lo que hay en file
        while (!in.atEnd())
        {
          QString line = in.readLine();     //lee linea por linea lo que hay en el archivo
          if(line==user && checkUser%2 ==0){//detecta que no exista el user name
              sameUser=true;

          }
          checkUser++;

        }
        inputFile.close();
    }
    if (sameUser==false){
        if(Pass2!=Pass){                //detecta que las contraseñas no sean distias
            warnPa=new QLabel();
            Registro->addWidget(warnPa);
            warnPa->setGeometry(700,500,150,50);
            warnPa->setText("la contraseña no coincide");
            warnPa->setStyleSheet("QLabel { background-color : transparent;color : red;}");
            warnPa->setFont(QFont("Arial"));


        }
        else{
            if (user==""){          //detecta que el usuario no este vacio
                warnUs=new QLabel();
                Registro->addWidget(warnUs);
                warnUs->setGeometry(700,500,150,50);
                warnUs->setText("no ha ingresado ningun usuario");
                warnUs->setStyleSheet("QLabel { background-color : transparent;color : red;}");
                warnUs->setFont(QFont("Arial"));
            }
            else{                   //escribe el usuario y la contraseña en file
                QFile file("file.txt");
                file.open(QIODevice::Append);
                QTextStream text(&file);
                text<<user<<"\r"<<"\n";
                text<<Pass<<"\r"<<"\n";
                file.close();
                QFile file2("posiciones.txt");
                file2.open(QIODevice::Append);
                QTextStream text2(&file2);
                text2<<user<<"\r"<<"\n";
                text2<<1<<"\r"<<"\n";
                text2<<75<<"\r"<<"\n";
                text2<<500<<"\n";
                file2.close();
                nivel=1;
                auxuser=user;
                Start();

            }



        }
    }
    else{                       //detecta que el usario ya existe
        warnUs=new QLabel();
        Registro->addWidget(warnUs);
        warnUs->setGeometry(700,500,150,50);
        warnUs->setText("este usuario ya existe");
        warnUs->setStyleSheet("QLabel { background-color : transparent;color : red;}");
        warnUs->setFont(QFont("Arial"));
    }




}

void MainWindow::LoginS()
{
  QString user;
  user=un->text();                         //retorna qstring con lo que haya en el cuadro.
  QString Pass;
  Pass=pw->text();
  QFile inputFile("file.txt");
  sameUser=false;
  checkUser=0;
  if (inputFile.open(QIODevice::ReadOnly))
  {
      QTextStream in(&inputFile);
      while (!in.atEnd())
      {
        QString line = in.readLine();
        qDebug()<<line;
        if (sameUser==true && line!=Pass){
            sameUser=false;
        }
        if(line==user && checkUser%2 ==0){
            sameUser=true;

        }
        checkUser++;

      }
      inputFile.close();
  }
  if (sameUser==true){
      if (user==""){
              warnUs=new QLabel();
              Login->addWidget(warnUs);
              warnUs->setGeometry(700,500,150,50);
              warnUs->setText("no ha ingresado ningun usuario");
              warnUs->setStyleSheet("QLabel { background-color : transparent;color : red;}");
              warnUs->setFont(QFont("Arial"));
          }

      else{
          auxuser=user;
          QFile inputFile("posiciones.txt");
          sameUser=false;
          checkUser=0;

          int s=0;
          if (inputFile.open(QIODevice::ReadOnly))//abre file en modo lectura
          {
              QTextStream in(&inputFile);         //capta lo que hay en file
              while (!in.atEnd())
              {
                QString line = in.readLine();     //lee linea por linea lo que hay en el archivo
                //qDebug()<<line;
                qDebug()<<"este es el usuario"<<auxuser;
                checkUser++;
                if(sameUser && s<=2){
                    qDebug()<<"entre en el cambio";
                    if(s==0){
                        //nivel=line.toInt();
                        qDebug()<<line.toInt();
                        nivel=line.toInt();
                    }
                    if(s==1){
                        //Trump->x=line.toInt();
                        qDebug()<<line.toInt();
                        auxpos[0]=line.toInt();
                    }
                    if(s==2){
                        //Trump->y=line.toInt();
                        sameUser=false;
                        qDebug()<<line.toInt();
                        auxpos[1]=line.toInt();
                    }
                    s++;
                }

                if(auxuser==line){
                    sameUser=true;
                    qDebug()<<"true";
                }
              }
              inputFile.close();
              Start();
          }
      }
  }


  else{
      warnUs=new QLabel();
      Login->addWidget(warnUs);
      warnUs->setGeometry(700,500,150,50);
      warnUs->setStyleSheet("QLabel { background-color : transparent;color : red;}");
      warnUs->setText("este usuario o contraseñas son invalidos");
      warnUs->setFont(QFont("Arial"));
  }

}

void MainWindow::BackToMenu() //Metodo para volver al menu principal
{
    //Escena menu
    menu=new QGraphicsScene(0,0,1080,700);

    ui->graphicsView->setScene(menu);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->adjustSize();
    ui->graphicsView->show();
    ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/imagenes/FondoT.png")));

    //Boton login
    login=new QPushButton();
    menu->addWidget(login);
    login->setGeometry(465,300,150,50);
    login->setText("Log in");
    login->setFont(QFont("Arial"));
    //Boton registro
    registro=new QPushButton();
    menu->addWidget(registro);
    registro->setGeometry(465,400,150,50);
    registro->setText("Registro");
    registro->setFont(QFont("Arial"));

    //Boton start temporal
    start=new QPushButton();
    menu->addWidget(start);
    start->setGeometry(465,500,150,50);
    start->setText("Guest");
    start->setFont(QFont("Arial"));

    //Boton multiplayer
    Multi=new QPushButton();
    menu->addWidget(Multi);
    Multi->setGeometry(465,600,150,50);
    Multi->setText("MultiPlayer");
    Multi->setFont(QFont("Arial"));

    connect(login,SIGNAL(released()),this,SLOT(MenuLogin()));
    connect(registro,SIGNAL(released()),this,SLOT(MenuRegistro()));
    connect(start,SIGNAL(released()),this,SLOT(Start()));
    connect(Multi,SIGNAL(released()),this,SLOT(MultiPlayer()));

}

void MainWindow::MultiPlayer()
{
    nivel=7;

    //Start();
    multip= new QGraphicsScene(this);

    multip->setSceneRect(0,0,1080,680);
    Trump=new jugador();
    Obama=new jugador();
    timer=new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
    timer->start(10);

    multip->addItem(Trump);
    multip->addItem(Obama);
    Obama->x=Trump->x+925;
    Obama->y=Trump->y;
    Obama->setPos(Obama->x,Obama->y);

    vida1=new QProgressBar();
    vida1->setGeometry(50,3,200,34);
    vida1->setValue(Trump->vida);
    vida1->setStyleSheet("QProgressBar { background-color : transparent;color : red;}");
    multip->addWidget(vida1);

    vida2=new QProgressBar();
    vida2->setGeometry(860,3,200,34);
    vida2->setValue(Trump->vida);
    vida2->setStyleSheet("QProgressBar { background-color : transparent;color : blue;}");
    multip->addWidget(vida2);

    Trump->setFlag(QGraphicsItem::ItemIsFocusable);
    Trump->setFocus();
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setScene(multip);
    ui->graphicsView->adjustSize();
    ui->graphicsView->show();
    ui->graphicsView->setBackgroundBrush(QBrush(QImage(":/imagenes/Fondo2.png")));
    //margenes
    l1=new QGraphicsLineItem(50,40,1025,40);
    l2=new QGraphicsLineItem(50,40,50,644);
    l3=new QGraphicsLineItem(50,644,1025,644);
    l4=new QGraphicsLineItem(1025,40,1025,644);
    multip->addItem(l1);//barra horizontal de arriba
    multip->addItem(l2);//barra vertical de la izquierda
    multip->addItem(l3);//barra horizontal de abajo
    multip->addItem(l4);//barra vertical de la derecha

}

void MainWindow::MultiPlayerV(char v)
{
    if (v=='T'){
        ganador=new QSplashScreen();
        ganador->setPixmap(QPixmap(":/imagenes/player1.jpg"));
        ganador->show();
    }
    else if(v=='O'){
        ganador=new QSplashScreen();
        ganador->setPixmap(QPixmap(":/imagenes/player2.jpg"));
        ganador->show();
    }
    timer_splash->singleShot(2500,ganador,SLOT(close()));

    timer->stop();
    //timer_splash->stop();
    nivel=1;

    BackToMenu();
}

void MainWindow::tiempo()
{
    lcd->display(auxtime);
    auxtime--;
}

void MainWindow::victoria()
{
    qDebug()<<"oe";

    victoriacampal=new QSplashScreen();
    victoriacampal->setPixmap(QPixmap(":/imagenes/victoriac.png"));
    victoriacampal->show();
    ganehpta->singleShot(5000,victoriacampal,SLOT(close()));
    auxtime=30;
    BackToMenu();
}

void MainWindow::crearbolas()
{

    qDebug()<<"cree una bola ...............";
    if(sis<2){
        Bolas *caida=new Bolas();
        bolas.append(caida);
        caida->especial=true;
        bolas.last()->setPos(caida->Px,caida->Py);
        etapa2->addItem(bolas.last());
        qDebug()<<"cree una bola ...............";
    }
    sis++;
    if (sis>=2){
        sis=0;
    }


}
void MainWindow::keyPressEvent(QKeyEvent *evento)
{
    if (nivel==1){
        qDebug()<<"parce funcine";

        if(evento->key()==Qt::Key_D){
                qDebug()<<"derecha";
                if (Trump->vx<6&&Trump->x<975){
                    Trump->vx+=4;
                }
                Trump->movimiento();
                /*x+=7;
                setPos(x,y);*/

                Trump->filas=0;
                Trump->columnas+=121;
                if(Trump->columnas>484){
                    Trump->columnas=0;
                }
                Trump->dir=1;
        }
        if(evento->key()==Qt::Key_A){
            qDebug()<<"izquierda";
            if (Trump->vx>-6&&Trump->x>100){
                Trump->vx-=4;
            }
            Trump->movimiento();
            /*x+=7;
            setPos(x,y);*/

            Trump->filas=141.25;
            Trump->columnas+=121;
            if(Trump->columnas>484){
                Trump->columnas=0;
            }
            Trump->dir=0;
            qDebug()<<Trump->dir;
        }
        if(evento->key()==Qt::Key_W){
            qDebug()<<"arriba";
            if(Trump->y>=100&&(Trump->djump<2)){
                Trump->vy=-10;
            }
            Trump->djump+=1;
            Trump->caida();

            if(Trump->dir==0){
                Trump->filas=141.25;
                Trump->columnas+=121;

            }
            else{
                Trump->filas=0;
                Trump->columnas+=121;

            }

            if(Trump->columnas>484){
                Trump->columnas=0;
            }
        }

        if(evento->key()==Qt::Key_S){
            qDebug()<<"disparo";

            balas.append(new bala());                           //agrega bala a la lista de balas
            etapa1->addItem(balas.last());
            //balas.last()->setPos(->x(),ev->y());
            if (Trump->dir){                                    //Le da la direccion a la lista de balas
                balas.last()->setPos(Trump->x+57,Trump->y+14);
            }
            else{
                balas.last()->setPos(Trump->x-57,Trump->y+14);
            }
            balas.last()->dir=Trump->dir;
        }
    }
    if (nivel==2){
        qDebug()<<"parce funcine";

        if(evento->key()==Qt::Key_D){
                qDebug()<<"derecha";
                if (Trump->vx<6&&Trump->x<975){
                    Trump->vx+=4;
                }
                Trump->movimiento();
                /*x+=7;
                setPos(x,y);*/

                Trump->filas=0;
                Trump->columnas+=121;
                if(Trump->columnas>484){
                    Trump->columnas=0;
                }
                Trump->dir=1;
        }
        if(evento->key()==Qt::Key_A){
            qDebug()<<"izquierda";
            if (Trump->vx>-6&&Trump->x>100){
                Trump->vx-=4;
            }
            Trump->movimiento();
            /*x+=7;
            setPos(x,y);*/

            Trump->filas=141.25;
            Trump->columnas+=121;
            if(Trump->columnas>484){
                Trump->columnas=0;
            }
            Trump->dir=0;
            qDebug()<<Trump->dir;
        }
        if(evento->key()==Qt::Key_W){
            qDebug()<<"arriba";
            if(Trump->y>=100&&(Trump->djump<2)){
                Trump->vy=-10;
            }
            Trump->djump+=1;
            Trump->caida();

            if(Trump->dir==0){
                Trump->filas=141.25;
                Trump->columnas+=121;

            }
            else{
                Trump->filas=0;
                Trump->columnas+=121;

            }

            if(Trump->columnas>484){
                Trump->columnas=0;
            }
        }


        if(evento->key()==Qt::Key_S){
            qDebug()<<"disparo";

            balas.append(new bala());                           //agrega bala a la lista de balas
            etapa2->addItem(balas.last());
            //balas.last()->setPos(->x(),ev->y());
            if (Trump->dir){                                    //Le da la direccion a la lista de balas
                balas.last()->setPos(Trump->x+57,Trump->y+14);
            }
            else{
                balas.last()->setPos(Trump->x-57,Trump->y+14);
            }
            balas.last()->dir=Trump->dir;
        }
    }
        if (nivel==7){
            if(evento->key()==Qt::Key_D){
                    qDebug()<<"derecha";
                    if (Trump->vx<6&&Trump->x<975){
                        Trump->vx+=4;
                    }
                    Trump->movimiento();
                    /*x+=7;
                    setPos(x,y);*/
                    Trump->filas=438.75;
                    Trump->columnas+=121;
                    if(Trump->columnas>484){
                        Trump->columnas=0;
                    }
                    Trump->dir=1;
            }
            if(evento->key()==Qt::Key_A){
                qDebug()<<"izquierda";
                if (Trump->vx>-6&&Trump->x>100){
                    Trump->vx-=4;
                }
                Trump->movimiento();
                /*x+=7;
                setPos(x,y);*/
                Trump->filas=297.5;
                Trump->columnas+=121;
                if(Trump->columnas>484){
                    Trump->columnas=0;
                }
                Trump->dir=0;
                qDebug()<<Trump->dir;
            }
            if(evento->key()==Qt::Key_W){
                qDebug()<<"arriba";
                if(Trump->y>=100&&(Trump->djump<2)){
                    Trump->vy=-10;
                }
                Trump->djump+=1;
                Trump->caida();
                if(Trump->dir==1){
                    Trump->filas=438.75;
                    Trump->columnas+=121;

                }
                else{
                    Trump->filas=297.5;
                    Trump->columnas+=121;

                }

                if(Trump->columnas>484){
                    Trump->columnas=0;
                }
            }

            if(evento->key()==Qt::Key_S){
                qDebug()<<"disparo";

                balas.append(new bala());                           //agrega bala a la lista de balas
                multip->addItem(balas.last());
                balas.last()->ws='T';
                //balas.last()->setPos(->x(),ev->y());
                if (Trump->dir){                                    //Le da la direccion a la lista de balas
                    balas.last()->setPos(Trump->x+57,Trump->y+14);
                }
                else{
                    balas.last()->setPos(Trump->x-57,Trump->y+14);
                }
                balas.last()->dir=Trump->dir;
            }

            /*//////////////////////////////////////////*/
            if(evento->key()==Qt::Key_L){
                    qDebug()<<"derecha";
                    if (Obama->vx<6&&Obama->x<975){
                        Obama->vx+=4;
                    }
                    Obama->movimiento();
                    /*x+=7;
                    setPos(x,y);*/
                    Obama->filas=0;
                    Obama->columnas+=121;
                    if(Obama->columnas>484){
                        Obama->columnas=0;
                    }
                    Obama->dir=1;
            }
            if(evento->key()==Qt::Key_J){
                qDebug()<<"izquierda";
                if (Obama->vx>-6&&Obama->x>100){
                    Obama->vx-=4;
                }
                Obama->movimiento();
                /*x+=7;
                setPos(x,y);*/
                Obama->filas=141.25;
                Obama->columnas+=121;
                if(Obama->columnas>484){
                    Obama->columnas=0;
                }
                Obama->dir=0;
                qDebug()<<Obama->dir;
            }
            if(evento->key()==Qt::Key_I){
                qDebug()<<"arriba";
                if(Obama->y>=100&&(Obama->djump<2)){
                    Obama->vy=-10;
                }
                Obama->djump+=1;
                Obama->caida();
                if(Obama->dir==0){
                    Obama->filas=141.25;
                    Obama->columnas+=121;

                }
                else{
                    Obama->filas=0;
                    Obama->columnas+=121;

                }

                if(Obama->columnas>484){
                    Obama->columnas=0;
                }
            }

            if(evento->key()==Qt::Key_K){
                qDebug()<<"disparo";

                balas.append(new bala());                           //agrega bala a la lista de balas
                multip->addItem(balas.last());
                balas.last()->ws='O';
                //balas.last()->setPos(->x(),ev->y());
                if (Obama->dir){                                    //Le da la direccion a la lista de balas
                    balas.last()->setPos(Obama->x+57,Obama->y+14);
                }
                else{
                    balas.last()->setPos(Obama->x-57,Obama->y+14);
                }
                balas.last()->dir=Obama->dir;
        }
        }

}



MainWindow::~MainWindow()
{
    delete ui;
}
