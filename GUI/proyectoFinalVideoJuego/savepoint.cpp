#include "savepoint.h"

SavePoint::SavePoint()
{
    pixmap= new QPixmap(":/imagenes/Save.png");
    x=500;
    y=150;
}

QRectF SavePoint::boundingRect() const
{
    return QRectF(-25,-25,50,50);
}

void SavePoint::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->drawPixmap(-25,-25,*pixmap,0,0,50,50);
}

