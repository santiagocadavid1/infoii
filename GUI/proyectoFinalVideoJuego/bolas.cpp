#include "bolas.h"

Bolas::Bolas()
{
    pixmap= new QPixmap(":/imagenes/bola.png");
    Radio=20;//5+rand()%(30-5+1);
    Masa =300;//50+rand()%(600-50+1);
    Friccion=0.05/100.0;
    CoeficienteRestitucion=1;
    Px=200+rand()%(700-200+1); //entre 60 y 920
    Py=500;
    Vx=30*pow(-1,rand()%2);
    Vy=0;
    rebotes=0;
    //Vy*=pow(-1,rand()%100);//Direccion de Velocidad en y
    Ax=0;
    Ay=0;
    NPiso=true;
}
void Bolas::CambioPosicion(){
    if(NPiso){
        Px+=Vx*DT+Ax*pow(DT,2)/2;
        Py+=Vy*DT+Ay*pow(DT,2)/2;
    }
    else Px+=Vx*DT;
}
void Bolas::CambioVelocidad(){
    if(NPiso)Vy=Vy+Ay*DT;
    Vx+=Ax*DT;
}
void Bolas::Aceleracion(){
    double r2,v2,ang;
    r2=pow(-Radio*1.0,2);
    v2=pow(Vx,2)+pow(Vy,2);
    if(int(Vx)==0)ang=90;
    else ang=atan2(Vy,Vx);
    if (NPiso)Ay=-Friccion*v2*r2*sin(ang)/Masa-G;
    Ax=-Friccion*v2*r2*cos(ang)/Masa;
}
void Bolas::ColisionX(){
    Vx*=-CoeficienteRestitucion;
}
void Bolas::ColisionY(bool x){
    Vy*=-CoeficienteRestitucion;
    if(abs(int(Vy))<(Radio*2)/3.0){
        //si colisiona con linea inferior
        if(x){
            NPiso=false;//esta en el piso (con poca velocidad)
            Py++;
            Vy=0;
        }
    }
    // si colisiona con linea superior
    if(!x){
        if(abs(int(Vy))<10)Vy=-(10+ rand()%30);
    }
}
QRectF Bolas::boundingRect() const{
    return QRectF(-Radio,-Radio,(Radio*2),(Radio*2));
}
void Bolas::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *){

    painter->drawPixmap(-20,-20,*pixmap,0,0,40,40);

}
Bolas::~Bolas(){}
