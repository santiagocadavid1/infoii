#include "bala.h"

bala::bala()
{
    pixmap= new QPixmap(":/imagenes/Bala.png");
    timer3=new QTimer();
    connect(timer3,SIGNAL(timeout()),this,SLOT(mover()));
    timer3->start(60);
    ;

}

QRectF bala::boundingRect() const
{
    return QRectF(-12/2,-12/2,12,12);
}

void bala::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->drawPixmap(-12/2,-12/2,*pixmap,0,0,12,12);
}

void bala::mover()                  //movimiento de la bala
{
    if (dir){
        setPos(x()+3,y()+0.1);
    }
    else{
        setPos(x()-3,y()+0.1);
    }
}

