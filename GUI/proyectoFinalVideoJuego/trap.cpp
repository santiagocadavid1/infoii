#include "trap.h"

Trap::Trap()
{
    timer4=new QTimer();
    direcc=1;
    pixmap= new QPixmap(":/imagenes/Trampa.png");
    x=260;
    y=100;
    vy=0;
    connect(timer4,SIGNAL(timeout()),this,SLOT(caida()));
    timer4->start(5000);
}

QRectF Trap::boundingRect() const
{
    return QRectF(-25,-25,50,50);
}

void Trap::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->drawPixmap(-25,-25,*pixmap,0,0,50,50);
}

void Trap::caida()
{
    if (y<100){
        direcc=1;

    }
    if(y>ymax){
        direcc=-1;

    }
    if (y<ymax){
    y=y+3*direcc;}
    else{
        y=y+1*(direcc);
    }
    setPos(x,y);

}

void Trap::choque()
{
    vy=-vy*1.0002;
}


