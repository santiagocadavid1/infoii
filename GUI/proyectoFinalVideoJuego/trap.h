#ifndef TRAP_H
#define TRAP_H
#include <QObject>
#include <QGraphicsItem>
#include <QPixmap>
#include <QPainter>
#include <QTimer>
#define g 0.05; //gravedad


class Trap : public QObject,public QGraphicsItem
{
        Q_OBJECT
public:
    float x;
    float y;
    Trap();
    QRectF boundingRect() const; //Contorno del personaje
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //pinta la trampa
    QPixmap *pixmap;
    float vy;
    void caida();
    void choque();
    QTimer *timer4;
    int direcc;
    int ymax;
};

#endif // TRAP_H
