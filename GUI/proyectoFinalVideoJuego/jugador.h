#ifndef JUGADOR_H
#define JUGADOR_H
//#include "bala.h"
#include <QObject>
#include <QGraphicsItem>
#include <QTimer>
#include <QPixmap>
#include <QPainter>
#include <QKeyEvent>
#define g 0.5  // gravedad
#define ff 0.5 // friccion
class jugador : public QObject,public QGraphicsItem
{
    Q_OBJECT
public:
    explicit jugador(QObject *parent = nullptr);

    void posicion(int v_limit);
    void choque();       //funcion para choque con el piso
    float vy;            //Velocidad en y
    float vx;            //Velocidad en x
    void caida();        //funcion para caida libre
    void movimiento();   //movimiento en x
    void muerte();
    int djump=0;          //doble salto

    QTimer *timer;       //timer de animar
    QPixmap *pixmap;     //Dibuja jugador

    float filas,columnas;//Iteran sobre el sprite
    float x;//posicion en x del jugador
    float y;//posicion en y del jugador
    float sp[2]={};
    float ancho;
    float alto;//dimensiones del jgador
    int vida=100;
    int a;
    QRectF boundingRect() const; //Contorno del personaje
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); //pinta el personaje
    bool dir=1;           //Booleano que vera a que direccion esta viendo el don gato (1 para derecha y 0 para izquierda)
    //bala *m45;

//signals:

//public slots:
    //void keyPressEvent(QKeyEvent *evento); //mando por medio del teclado
};

#endif // JUGADOR_H
