#ifndef OBJETO_H
#define OBJETO_H

#include <QObject>
#include <QGraphicsPixmapItem>

class Objeto : public QObject,
                public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Objeto(QGraphicsItem* carr = 0);
    void posicion(int v_limit);
    int Px=0;
    int Py=0;
signals:

public slots:
};

#endif // OBJETO_H
