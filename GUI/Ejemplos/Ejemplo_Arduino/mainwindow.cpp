#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer_control=new QTimer;
    control = new QSerialPort(this);
    scene=new QGraphicsScene(this);         //crea la scene
    scene->setSceneRect(0,0,500,400);     //asigna el rectangulo que encierra la scene, determinado por h_limit y v_limit
    ui->graphicsView->setScene(scene);
    connect(timer_control,SIGNAL(timeout()),this,SLOT(Joy()));
    carro= new Objeto();
    scene->addItem(carro);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionConnect_triggered()
{
    control->setPortName("COM5");
    //timer_control->start(100);
    if(control->open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!control->setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<control->errorString();

        if(!control->setDataBits(QSerialPort::Data8))
            qDebug()<<control->errorString();

        if(!control->setParity(QSerialPort::NoParity))
            qDebug()<<control->errorString();

        if(!control->setStopBits(QSerialPort::OneStop))
            qDebug()<<control->errorString();

        if(!control->setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<control->errorString();
            timer_control->start(20);
    }
    else{
        qDebug()<<"Serial COM3 not opened. Error: "<<control->errorString();
    }
}
void MainWindow::Joy(){
        char data;
        int l = 0;
        bool flag=true;
           if(control->waitForReadyRead(100)){

                //Data was returned
                l = control->read(&data,1);
                switch (data) {
                case 'A':
                      carro->Px=carro->Px-2;
                    break;
                case 'B':
                      carro->Px=carro->Px+2;
                    break;
                case 'C':
                      carro->Py=carro->Py+2;
                    break;
                case 'D':
                      carro->Py=carro->Py-2;
                    break;
                case 'K':
                    timer_control->stop();
                    control->close();
                    //close();
                    break;
                case 'N':
                    break;
                default:
                    break;
                }
                carro->posicion(400);
                qDebug()<<"Response: "<<data;
                flag=false;

            }else{
                //No data
                qDebug()<<"Time out";
          }
        //}
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //control->close();

}
