#include <iostream>

using namespace std;

//Ejercicios

//Ejercicio 1
/*
int main()
{
    int A;
    int B;
    cout<<"Ingrese el primer numero: "<<endl;
    cin>>A;
    cout<<"Ingrese el segundo numero: "<<endl;
    cin>>B;
    cout<<"El residuo de la division "<<A<<"/"<<B<<" es: "<< A%B <<endl;
    return 0;
} */

//Ejercicio 2
/*
int main(){
    int N;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    if(N%2==0){
        cout<<N<<" es par"<<endl;
    }
    else cout<<N<<" es impar"<<endl;
    return 0;
} */

//Ejercicio 3
/*
int main(){
    int a;
    int b;
    cout<<"Ingrese el primer numero: "<<endl;
    cin>>a;
    cout<<"Ingrese el segundo numero: "<<endl;
    cin>>b;
    if (a>=b){
        cout<<a<<" es el mayor"<<endl;
    }
    else {
        cout<<b<<" es el mayor"<<endl;
    }
    return 0;
} */

//Ejercicio 4
/*
int main(){
    int a;
    int b;
    cout<<"Ingrese el primer numero: "<<endl;
    cin>>a;
    cout<<"Ingrese el segundo numero: "<<endl;
    cin>>b;
    if (a>=b){
        cout<<b<<" es el menor"<<endl;
    }
    else {
        cout<<a<<" es el menor"<<endl;
    }
    return 0;
} */

//Ejercicio 5
/*
int main(){
    int a;
    int b;
    int div;
    float res;
    cout<<"Ingrese el primer numero "<<endl;
    cin>>a;
    cout<<"Ingrese le segundo numero "<<endl;
    cin>>b;
    div=a/b;
    res=float((a%b))/float(b);
    if (res>=0.5){
        div++;
    }
    cout<<"La division "<<a<<"/"<<b<<" con redondeo es "<<div<<endl;
    return 0;
} */

//Ejercicio 6
/*
int main(){
    int A;
    int B;
    int pot;
    cout<<"Ingrese el primer numero "<<endl;
    cin>>A;
    cout<<"Ingrese el segundo numero "<<endl;
    cin>>B;
    pot=A;
    for(int i=1;i<B;i++){
        pot=pot*A;
    }
    cout<<A<<"^"<<B<<"="<<pot<<endl;
    return 0;
} */

//Ejercicio 7
/*
int main(){
    int a;
    int sum=0;
    cout<<"Ingrese un numero "<<endl;
    cin>>a;
    for(int i=0;i<=a;i++){
        sum=sum+i;
    }
    cout<<"La sumatoria desde 0 hasta "<<a<<" es "<<sum<<endl;
    return 0;
} */

//Ejercicio 8
/*
int main(){
    int a;
    int fact=1;
    cout<<"Ingrese un numero "<<endl;
    cin>>a;
    for(int i=1;i<=a;i++){
        fact=fact*i;
    }
    cout<<"El factorial de "<<a<<" es "<<fact<<endl;
    return 0;
}*/

//Ejercicio 9
/*
int main(){
    float r;
    cout<<"Ingrese un numero "<<endl;
    cin>>r;
    cout<<"El perimetro de un circulo de radio "<<r<<" es "<<(3.1416)*2*r<<endl;
    cout<<"El area de un circulo de radio "<<r<<" es "<<(3.1416)*(r)*(r)<<endl;
    return 0;
} */

//Ejercicio 10
/*
int main(){
    int N;
    int mult;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    mult=N;
    cout<<"Los multiplos de "<<N<<" menores que 100 son: "<<endl;
    while (mult>=1&&mult<=100){
        cout<<mult<<endl;
        mult=mult+N;
    }
    return 0;
} */

//Ejercicio 11
/*
int main(){
    int N;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    for(int i=1;i<=10;i++){
        cout<<i<<"x"<<N<<"="<<i*N<<endl;
    }
    return 0;
} */

//Ejercicio 12
/*
int main(){
    int N;
    int pot;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    pot=N;
    for(int i=1;i<=5;i++){
        for(int j=1;j<i;j++){
            pot=pot*N;
        }
        cout<<N<<"^"<<i<<"="<<pot<<endl;
        pot=N;
    }
} */

//Ejercicio 13
/*
int main(){
    int N;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    for(int i=1;i<=N;i++){
        if(N%i==0){
            cout<<i<<endl;
        }
    }
    return 0;
} */

//Ejercicio 14
/*
int main(){
    for(int i=1;i<=50;i++){
        cout<<i<<"   "<<51-i<<endl;
    }
    return 0;
} */

//Ejercicio 15
/*
int main(){
    int a=1;
    int sum=0;
    while (a!=0){
        cout<<"Ingrese un numero "<<endl;
        cin>>a;
        sum=sum+a;
    }
    cout<<"El resultado de la sumatoria es: "<<sum<<endl;
    return 0;
}*/

//Ejercicio 16
/*
int main(){
    int a=1;
    int con=0;
    int sum=0;
    int av;
    while(a!=0){
        cout<<"Ingrese un numero "<<endl;
        cin>>a;
        con++;
        sum=sum+a;
    }
    con--;
    av=sum/con;
    cout<<"El primedio es: "<<av<<endl;
} */

//Ejercicio 17
/*
int main(){
    int a=1;
    int b=0;
    while(a!=0){
        cout<<"Ingrese un numero "<<endl;
        cin>>a;
        if (a>b){
            b=a;
        }
    }
    cout<<"El mayor numero ingresado fue "<<b<<endl;
    return 0;
} */

//Ejercicio 18
/*
int main(){
    int a;
    bool flag=false;
    cout<<"Ingrese un numero "<<endl;
    cin>>a;
    for(int i=0;i<=(a/2);i++){
        if(i*i==a){
            flag=true;
            break;
        }
    }
    if (flag){
        cout<<a<<" es cuadrado perfecto"<<endl;
    }
    else{
        cout<<a<<" no es cuadrado perfecto"<<endl;
    }
    return 0;
} */

//Ejercicio 19
/*
int main(){
    int N;
    int divs=0;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    for(int i=N;i>=(N/2);i--){
        if (N%i==0){
            divs++;
        }
    }
    if (divs==2){
        cout<<N<<" es primo"<<endl;
    }
    else {
        cout<<N<<" no es primo"<<endl;
    }
    return 0;
} */

//Ejercicio 20
/*
int main(){
    int N;
    int n=0;
    int aux;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    aux=N;
    while(aux!=0){
        n=(n+(aux%10))*10;
        aux=aux/10;
    }
    n=n/10;
    if (N==n){
        cout<<N<<" es palindromo"<<endl;
    }
    else{
        cout<<N<<" no es palindromo"<<endl;
    }
    return 0;
} */

//Ejercicio 21
/*
int main(){
    char C;
    cout<<"Ingrese una letra "<<endl;
    cin>>C;
    if(C>=65&&C<=90){
        cout<<"La letra convertida es: "<<char(C+32)<<endl;
    }
    else if (C>=97&&C<=122){
        cout<<"La letra convertida es: "<<char(C-32)<<endl;
    }
    else{
        cout<<"El caracter ingresado no es una letra"<<endl;
    }
    return 0;
} */

//Ejercicio 22
/*
int main(){
    int s;
    int sec;
    int min;
    int hour;
    cout<<"Ingrese un numero entero de segundos "<<endl;
    cin>>s;
    hour=s/3600;
    s=s%3600;
    min=s/60;
    s=s%60;
    sec=s;
    cout<<hour<<":"<<min<<":"<<sec<<endl;
    return 0;
} */

//Ejercicio 23
/*
int main(){
    int a;
    int b;
    int mcm;
    int mcd=1;
    int bg;
    cout<<"Ingrese el primer numero "<<endl;
    cin>>a;
    cout<<"Ingrese el segundo numero "<<endl;
    cin>>b;
    if (a>b){
        bg=a;
    }
    else {
        bg=b;
    }
    for (int i=2;i<=(bg/2)+1;i++)
        if(a%i==0&&b%i==0){
            mcd=i;
            break;
        }
    mcm=a*b/mcd;
    cout<<"El minimo comun multiplo entre "<<a<<" y "<<b<<" es "<<mcm<<endl;
    return 0;
} */

//Ejercicio 24
/*
int main(){
    int n;
    cout<<"Ingrese un numero "<<endl;
    cin>>n;
    for(int i=1;i<=n;i++){
        cout<<"+";
    }
    cout<<endl;
    for(int j=1;j<=(n-2);j++){
        cout<<"+";
        for(int k=1;k<=(n-2);k++){
            cout<<" ";
        }
        cout<<"+"<<endl;
    }
    for(int i=1;i<=n;i++){
        cout<<"+";
    }
    cout<<endl;
    return 0;
} */

//Ejercicio 25
/*
int main(){
    int N;
    int aux;
    int con=0;
    cout<<"Ingrese un numero "<<endl;
    cin>>N;
    aux=N;
    while(aux!=0){
        aux=aux/10;
        con++;
    }
    cout<<"El numero de digitos de "<<N<<" es "<<con<<endl;
    return 0;
} */

//Ejercicio 26
/*
int main(){
    int a;
    int b;
    int c;
    bool flag=true;
    cout<<"Ingrese el primer lado del triangulo "<<endl;
    cin>>a;
    cout<<"Ingrese el segundo lado del triangulo "<<endl;
    cin>>b;
    cout<<"Ingrese el tercer lado del triangulo "<<endl;
    cin>>c;
    if ((a+b<=c)||(a+c<=b)||(b+c<=a)){
        flag=false;
    }
    if (flag){
        if (a==b&&b==c){
            cout<<"El triangulo es equilatero"<<endl;
        }
        else if ((a==b&&b!=c)||(a==c&&c!=b)||(b==c&&b!=a)){
            cout<<"El triangulo es isoseles"<<endl;
        }
        else {
            cout<<"El triangulo es escaleno"<<endl;
        }
    }
    else {
        cout<<"Las longitudes ingresadas no forman un triangulo"<<endl;
    }
    return 0;
} */

//Ejercicio 27
/*
int main(){
    int a;
    int b;
    char c;
    cout<<"Ingrese el primer numero "<<endl;
    cin>>a;
    cout<<"Ingrese el operador "<<endl;
    cin>>c;
    cout<<"Ingrese el segundo numero "<<endl;
    cin>>b;
    if (c=='+'){
        cout<<a<<"+"<<b<<"="<<a+b<<endl;
    }
    else if (c=='-'){
        cout<<a<<"-"<<b<<"="<<a-b<<endl;
    }
    else if (c=='*'){
        cout<<a<<"*"<<b<<"="<<a*b<<endl;
    }
    else if (c=='/'){
        cout<<a<<"/"<<b<<"="<<a/b<<endl;
    }
    else {
        cout<<"El operador ingresado no es valido"<<endl;
    }
    return 0;
} */

//Ejercicio 28
/*
int main(){
    int n;
    float pi;
    float aux=0;
    float min=-1;
    cout<<"Ingrese un numero: "<<endl;
    cin>>n;
    for(int i=0;i<n;i++){
        min=min*(-1);
        aux=aux+(min*float(float(1)/float(1+2*i)));
    }
    pi=4*aux;
    cout<<"Pi es aproximadamente: "<<pi<<endl;
    return 0;
} */

//Ejercicio 29
/*
#include <cstdlib>

int main(){
    int A;
    int B;
    char K;
    int max=100;
    int min=0;
    bool flag=true;
    cout<<"Ingrese el numero a adivinar "<<endl;
    cin>>A;
    B=rand() % (max+1-min) + min;
    while(flag){
        cout<<B<<endl;
        cout<<"Ingrese < si el numero que busca es menor, > si el numero que busca es mayor o = si se ha adivinado su numero"<<endl;
        cin>>K;
        if (K=='<'){
            max=B;
            B=rand() % (max-min) + min;
        }
        else if (K=='>'){
            min=B;
            B=rand() % (max+1-min) + (min+1);
        }
        else {
            flag=false;
        }
    }
    cout<<"Usted ingreso "<<A<<" y el numero adivinado fue "<<B<<endl;
    return 0;
} */

//Ejercicio 30
/*
#include <cstdlib>
#inculde <time.h>
int main(){
    int A;
    int B=101;
    bool flag=true;
    int con=0;
    A=rand(time(0)) % 101;
    while (flag){
        cout<<"Adivine el numero: "<<endl;
        cin>>B;
        con++;
        if (B<A){
            cout<<"El numero que ingreso es menor que el que busca "<<endl;
        }
        else if (B>A){
            cout<<"El numero que ingreso es mayor que el que busca "<<endl;
        }
        else {
            cout<<"Adivino el numero"<<endl;
            flag=false;
        }
    }
    cout<<"El numero a adivinar era "<<A<<" el numero que ingreso fue "<<B<<" y lo adivino en el intento "<<con<<endl;
    return 0;
} */

//Problema 1

/*
bool compara(char *letras, char letra);

int main(){
    char l;
    char vocales[]={'a','e','i','o','u','A','E','I','O','U',0};

    cout<<"Digite una letra "<<endl;
    cin>>l;

    if ((l>='A'&&l<='Z')||(l>='a'&&l<='z')){
        if (compara(vocales,l)){
            cout<<"La letra "<<l<<" es una vocal "<<endl;
        }
        else {
            cout<<"La letra "<<l<<" no es una vocal"<<endl;
        }
    }
    else{
        cout<<l<<" no es una letra"<<endl;
    }
    return 0;
}

bool compara(char *letras, char letra){
    while(*letras!=0){
        if (letra==*letras){
            return true;
        }
        letras=letras+1;
    }
    return false;
} */

//Problema 2
/*
int main(){
    int D;
    int bill[]={50000,20000,10000,5000,2000,1000,500,200,100,50,0};
    int *pbill;
    pbill=bill;
    cout<<"Ingrese una cantidad de dinero: "<<endl;
    cin>>D;
    while(*pbill!=0){
        cout<<*pbill<<": "<<D/(*pbill)<<endl;
        D=D%(*pbill);
        pbill=pbill+1;
    }
    cout<<"Faltante: "<<(D)<<endl;
    return 0;
} */

//Problema 3
/*
int main(){
    int d;
    int m;
    int ml[]={1,3,5,7,8,10,12};
    int mc[]={4,6,10,12};
    cout<<"Ingrese un dia"<<endl;
    cin>>d;
    cout<<"Ingrese un mes"<<endl;
    cin>>m;
    for (int i=0;i<=6;i++){
        if(m==ml[i]){
            if(d<=31&&d>=1){
               cout<<d<<"/"<<m<<" es valido"<<endl;
            }
            else{
               cout<<d<<"/"<<m<<" no es valido"<<endl;
            }
            return 0;
        }
    }
    for (int i=0;i<=3;i++){
        if(m==mc[i]){
            if(d<=30&&d>=1){
               cout<<d<<"/"<<m<<" es valido"<<endl;
            }
            else{
               cout<<d<<"/"<<m<<" no es valido"<<endl;
            }
            return 0;
        }
    }
    if ((m==2)&&(d==29)){
        cout<<d<<"/"<<m<<" es valido en biciesto"<<endl;
    }
    else if (m==2){
        if(d<=28&&d>=1){
            cout<<d<<"/"<<m<<" es valido"<<endl;
        }
        else{
            cout<<d<<"/"<<m<<" no es valido"<<endl;
        }
    }
    else {
        cout<<d<<"/"<<m<<" no es valido"<<endl;
    }
    return 0;
}*/

//Problema 4
/*
int main(){
    int a=0;
    int b=0;
    int s=0;
    cout<<"Digite la primera hora ";
    cin>>a;
    cout<<"Digite la segunda hora ";
    cin>>b;
    if ((a/100)>=24||(a%100>=60)||a<0){
        cout<<a<<" es un tiempo invalido.";
        return 0;
    }
    else if ((b/100)>=24||(b%100>=60)||b<0){
        cout<<b<<" es un tiempo invalido.";
        return 0;
    }
    s=((((a/100)+(b/100))%24)*100)+((((a%100)+(b%100))/60)*100)+((((a%100)+(b%100)))%60);
    cout<<"La hora es "<<s<<endl;
    return 0;
} */

//Problema 5
/*
int main()
{
    int a=2,i=0,x=0,ca=0,ce=0,k=0;
    while ((a%2)==0){
    cout << "digite  el largo del arreglo solo impares" << endl;
    cin>>a;
    x=(a+1)/2;
    }
    for (i=1;i<=x;i+=1){
        ca=1+(2*(i-1));
        ce=(a-ca)/2;
        for (k=1;k<=ce;k++){
            cout<<" ";
        }
        for (k=1;k<=ca;k++){
            cout<<"*";
        }
        for (k=1;k<=ce;k++){
            cout<<" ";
        }
        cout<<endl;
   }
   for (i=1;i<x;i+=1){
        ca=ca-2;
        ce=(a-ca)/2;
        for (k=1;k<=ce;k++){
            cout<<" ";
        }
        for (k=1;k<=ca;k++){
            cout<<"*";
        }
        for (k=1;k<=ce;k++){
            cout<<" ";
        }
        cout<<endl;
   }
    return 0;
} */

//Problema 6
/*
int main(){
    int n=0;
    cout<<"Ingrese el numero de elementos a usar en la aproximacion "<<endl;
    cin>>n;
    while(n<=0){
        cout<<"Ingrese un numero valido"<<endl;
        cout<<"Ingrese el numero de elementos a usar en la aproximacion "<<endl;
        cin>>n;
    }
    double e=1;
    int aux=1;
    double resultado=1;
    if(n==1){cout<<"el valor aproximado de e es: "<<1<<endl;}
    else {
        for(int i=1;i<=(n-1);i++){
            for( int j=1;j<=aux;j++){
                resultado=resultado*j;
                }
            e= e+ (1/resultado);
            aux=aux+1;
            resultado=1;
        }
        cout<<"e es aproximadamente "<<e<<endl;
    }
    return 0;
} */

//Problema 7
/*
int main(){
    int n=0;
    int a=1;
    int b=0;
    int c=1;
    int sum=0;
    cout<<"Digite un numero ";
    cin>>n;
    while (c<n){
        if ((c%2)==0){
            sum=sum+c;
        }
        c=a+b;
        b=a;
        a=c;
   }
   cout<<"El resultado de la suma es: "<<sum<<endl;
   return 0;
} */

//Problema 8
/*
int main(){
    int a=0;
    int b=0;
    int c=0;
    int aux=0;
    int con=1;
    int sum=0;
    cout<<"Digite los 3 numeros separados por enter ";
    cin>>a>>b>>c;
    cout<<"Se sumarian ";
    while (aux<c){
        sum=sum+aux;
        cout<<aux<<"+";
        aux=a*con;
        con++;
    }
    aux=0;
    con=1;
    while (aux<c){
        if (aux%a!=0){
        sum=sum+aux;
        cout<<aux<<"+";
        }
        aux=b*con;
        con++;
    }
    cout<<"="<<sum<<endl;
    return 0;
}*/

//Problema 9
/*
#include <math.h>
int main(){
   int num,c=0;
   cout<<"Ingrese un numero "<<endl;
   cin>>num;
   while (num >=10){
        c=c+pow((num%10),(num%10));
        num=num/10;}
   c=c+pow(num,num);
   cout<<"La suma de sus digitos elevados a si mismos es "<<c<<endl;
   return 0;
} */

//Problema 10
/*
int main(){
    int n=0;
    int c=0;
    int p=2;
    int pr=0;
    cout<<"Digite un numero ";
    cin>>n;
    while (c<n){
        pr=0;
        for (int i=1;i<=p;i=i+1){
           if ((p%i)==0){
               pr=pr+1;}
        }
        if (pr==2){
            c=c+1;
            if (c==n){
                break;
            }
        }
        p=p+1;
    }
    cout<<"El primo numero "<<n<<" es "<<p<<endl;
    return 0;
} */

//Problema 11
/*
int main(){
    bool v=true;
    int a=0;
    int i=0;
    cout<<"Digite un numero ";
    cin>>a;
    while (v){
        i=i+1;
        for (int j=1;j<=a;j=j+1){
            if (i%j!=0){
            v=true;
            break;}
        v=false;}
        }
    cout<<"El minimo comun multiplo es "<<i<<endl;
    return 0;
} */

//Problema 12
/*
int primo(int n);
int main(){
    int n,max=0;
    cout<<"Ingrese un numero "<<endl;
    cin>>n;
    n=abs(n);
    for(int i=n;i>=1;i--){
        if (n%i==0){
            int div= n/i;
            if (primo(div)){
                max =div;
            }
        }
    }
    cout<<"el primo mas grande es: "<<max<<endl;
    return 0;
}
int primo(int n){
    int r=0;
    for(int i=1; i<=n; i++){
        if(n%i==0){
            r++;
        }
    }
    if(r==2){
        return true;
    }
    else{
        return false;
    }
} */

//Problema 13
/*
int main(){
    int n=0;
    int sum=0;
    int pr=0;
    cout<<"Digite un numero ";
    cin>>n;
    for (int i=2;i<n;i=i+1){
        pr=0;
        for (int j=1;j<=i;j=j+1){
            if ((i%j)==0){
                pr=pr+1;
            }
        }
        if (pr==2){
            sum=sum+i;
        }
    }
    cout<<"El resultado de la suma es: "<<sum<<endl;
    return 0;
} */

//Problema 14
/*
bool esPalindromo(unsigned long origen);
int main() {
  unsigned int  maximo = 0;
  int nm=0,mm=0;

  for (int n=100;n<=999;n++) {
    for (int m=100;m<=999;m++) {
      unsigned int p = n*m;
      if(esPalindromo(p) && p > maximo) {
        maximo = p;
        nm=n;
        mm=m;
      }
    }
  }
  cout<<nm<<"*"<<mm<<"="<<maximo<<endl;
  return 0;
}

bool esPalindromo(unsigned long origen) {
  unsigned long reversed = 0, n = origen;

  while (n > 0) {
    reversed = reversed * 10 + n % 10;
    n /= 10;
  }
  return (reversed==origen);
} */

//Problema 15
/*
int main(){
    int k,acum=1,total=2,acumt=1;
cout<<"digite el ancho de  matriz ";
cin>>k;
  for(int i=1;i<k;i=i+2){
    for(int j=0;j<4;j++){
        acum=acum+total;
        acumt=acum+acumt;
    }
  total=total+2;}
  cout<<"En una espiral de "<<k<<"x"<<k<<" , la suma es: "<<acumt<<endl;
} */

//Problema 16
/*
int main(){
    int k=0;
    int c=0;
    int cm=0;
    int jm=0;
    cout<<"Digite un numero ";
    cin>>k;
    for(int j=1;j<=k;j=j+1){
        c=0;
        int ji=j;
        while (ji!=1){
            if ((ji%2)==0){
                ji=ji/2;
            }
            else{
                ji=(ji*3)+1;
            }
            c=c+1;
        }
        if (c>=cm){
            jm=j;
            cm=c;
        }
    }
    cout<<"La serie mas larga es con la semilla: "<<jm<<", teniendo "<<cm<<" terminos."<<endl;
    return 0;
} */

//Problema 17
/*
int main(){
    int k,x=1,barr=0,divs=0;
    cout<<"digite el numero de divisores";
    cin>>k;
    while (divs<=k){
        barr=x*(x+1)/2;
        divs=0;
        for (int i=1;i<=barr;i++){
            if ((barr%i)==0){
                divs=divs+1;
            }}
        if (divs>k){
            cout<<"el numero es : "<<barr<<" y tiene "<<divs<<" divisores"<<endl;
        break;}
        x=x+1;
    }
} */

